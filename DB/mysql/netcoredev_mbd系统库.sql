/*
 Navicat Premium Data Transfer

 Source Server         : 120.53.251.208
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : 120.53.251.208:3306
 Source Schema         : netcoredev_mbd

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 22/02/2023 00:51:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Sys_City
-- ----------------------------
DROP TABLE IF EXISTS `Sys_City`;
CREATE TABLE `Sys_City`  (
  `CityId` int(11) NULL DEFAULT NULL,
  `CityCode` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CityName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ProvinceCode` text CHARACTER SET utf8 COLLATE utf8_bin NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_City
-- ----------------------------
INSERT INTO `Sys_City` VALUES (1, '110100', '市辖区', '110000');
INSERT INTO `Sys_City` VALUES (2, '110200', '县', '110000');
INSERT INTO `Sys_City` VALUES (3, '120100', '市辖区', '120000');
INSERT INTO `Sys_City` VALUES (4, '120200', '县', '120000');
INSERT INTO `Sys_City` VALUES (5, '130100', '石家庄市', '130000');
INSERT INTO `Sys_City` VALUES (6, '130200', '唐山市', '130000');
INSERT INTO `Sys_City` VALUES (7, '130300', '秦皇岛市', '130000');
INSERT INTO `Sys_City` VALUES (8, '130400', '邯郸市', '130000');
INSERT INTO `Sys_City` VALUES (9, '130500', '邢台市', '130000');
INSERT INTO `Sys_City` VALUES (10, '130600', '保定市', '130000');
INSERT INTO `Sys_City` VALUES (11, '130700', '张家口市', '130000');
INSERT INTO `Sys_City` VALUES (12, '130800', '承德市', '130000');
INSERT INTO `Sys_City` VALUES (13, '130900', '沧州市', '130000');
INSERT INTO `Sys_City` VALUES (14, '131000', '廊坊市', '130000');
INSERT INTO `Sys_City` VALUES (15, '131100', '衡水市', '130000');
INSERT INTO `Sys_City` VALUES (16, '140100', '太原市', '140000');
INSERT INTO `Sys_City` VALUES (17, '140200', '大同市', '140000');
INSERT INTO `Sys_City` VALUES (18, '140300', '阳泉市', '140000');
INSERT INTO `Sys_City` VALUES (19, '140400', '长治市', '140000');
INSERT INTO `Sys_City` VALUES (20, '140500', '晋城市', '140000');
INSERT INTO `Sys_City` VALUES (21, '140600', '朔州市', '140000');
INSERT INTO `Sys_City` VALUES (22, '140700', '晋中市', '140000');
INSERT INTO `Sys_City` VALUES (23, '140800', '运城市', '140000');
INSERT INTO `Sys_City` VALUES (24, '140900', '忻州市', '140000');
INSERT INTO `Sys_City` VALUES (25, '141000', '临汾市', '140000');
INSERT INTO `Sys_City` VALUES (26, '141100', '吕梁市', '140000');
INSERT INTO `Sys_City` VALUES (27, '150100', '呼和浩特市', '150000');
INSERT INTO `Sys_City` VALUES (28, '150200', '包头市', '150000');
INSERT INTO `Sys_City` VALUES (29, '150300', '乌海市', '150000');
INSERT INTO `Sys_City` VALUES (30, '150400', '赤峰市', '150000');
INSERT INTO `Sys_City` VALUES (31, '150500', '通辽市', '150000');
INSERT INTO `Sys_City` VALUES (32, '150600', '鄂尔多斯市', '150000');
INSERT INTO `Sys_City` VALUES (33, '150700', '呼伦贝尔市', '150000');
INSERT INTO `Sys_City` VALUES (34, '150800', '巴彦淖尔市', '150000');
INSERT INTO `Sys_City` VALUES (35, '150900', '乌兰察布市', '150000');
INSERT INTO `Sys_City` VALUES (36, '152200', '兴安盟', '150000');
INSERT INTO `Sys_City` VALUES (37, '152500', '锡林郭勒盟', '150000');
INSERT INTO `Sys_City` VALUES (38, '152900', '阿拉善盟', '150000');
INSERT INTO `Sys_City` VALUES (39, '210100', '沈阳市', '210000');
INSERT INTO `Sys_City` VALUES (40, '210200', '大连市', '210000');
INSERT INTO `Sys_City` VALUES (41, '210300', '鞍山市', '210000');
INSERT INTO `Sys_City` VALUES (42, '210400', '抚顺市', '210000');
INSERT INTO `Sys_City` VALUES (43, '210500', '本溪市', '210000');
INSERT INTO `Sys_City` VALUES (44, '210600', '丹东市', '210000');
INSERT INTO `Sys_City` VALUES (45, '210700', '锦州市', '210000');
INSERT INTO `Sys_City` VALUES (46, '210800', '营口市', '210000');
INSERT INTO `Sys_City` VALUES (47, '210900', '阜新市', '210000');
INSERT INTO `Sys_City` VALUES (48, '211000', '辽阳市', '210000');
INSERT INTO `Sys_City` VALUES (49, '211100', '盘锦市', '210000');
INSERT INTO `Sys_City` VALUES (50, '211200', '铁岭市', '210000');
INSERT INTO `Sys_City` VALUES (51, '211300', '朝阳市', '210000');
INSERT INTO `Sys_City` VALUES (52, '211400', '葫芦岛市', '210000');
INSERT INTO `Sys_City` VALUES (53, '220100', '长春市', '220000');
INSERT INTO `Sys_City` VALUES (54, '220200', '吉林市', '220000');
INSERT INTO `Sys_City` VALUES (55, '220300', '四平市', '220000');
INSERT INTO `Sys_City` VALUES (56, '220400', '辽源市', '220000');
INSERT INTO `Sys_City` VALUES (57, '220500', '通化市', '220000');
INSERT INTO `Sys_City` VALUES (58, '220600', '白山市', '220000');
INSERT INTO `Sys_City` VALUES (59, '220700', '松原市', '220000');
INSERT INTO `Sys_City` VALUES (60, '220800', '白城市', '220000');
INSERT INTO `Sys_City` VALUES (61, '222400', '延边朝鲜族自治州', '220000');
INSERT INTO `Sys_City` VALUES (62, '230100', '哈尔滨市', '230000');
INSERT INTO `Sys_City` VALUES (63, '230200', '齐齐哈尔市', '230000');
INSERT INTO `Sys_City` VALUES (64, '230300', '鸡西市', '230000');
INSERT INTO `Sys_City` VALUES (65, '230400', '鹤岗市', '230000');
INSERT INTO `Sys_City` VALUES (66, '230500', '双鸭山市', '230000');
INSERT INTO `Sys_City` VALUES (67, '230600', '大庆市', '230000');
INSERT INTO `Sys_City` VALUES (68, '230700', '伊春市', '230000');
INSERT INTO `Sys_City` VALUES (69, '230800', '佳木斯市', '230000');
INSERT INTO `Sys_City` VALUES (70, '230900', '七台河市', '230000');
INSERT INTO `Sys_City` VALUES (71, '231000', '牡丹江市', '230000');
INSERT INTO `Sys_City` VALUES (72, '231100', '黑河市', '230000');
INSERT INTO `Sys_City` VALUES (73, '231200', '绥化市', '230000');
INSERT INTO `Sys_City` VALUES (74, '232700', '大兴安岭地区', '230000');
INSERT INTO `Sys_City` VALUES (75, '310100', '市辖区', '310000');
INSERT INTO `Sys_City` VALUES (76, '310200', '县', '310000');
INSERT INTO `Sys_City` VALUES (77, '320100', '南京市', '320000');
INSERT INTO `Sys_City` VALUES (78, '320200', '无锡市', '320000');
INSERT INTO `Sys_City` VALUES (79, '320300', '徐州市', '320000');
INSERT INTO `Sys_City` VALUES (80, '320400', '常州市', '320000');
INSERT INTO `Sys_City` VALUES (81, '320500', '苏州市', '320000');
INSERT INTO `Sys_City` VALUES (82, '320600', '南通市', '320000');
INSERT INTO `Sys_City` VALUES (83, '320700', '连云港市', '320000');
INSERT INTO `Sys_City` VALUES (84, '320800', '淮安市', '320000');
INSERT INTO `Sys_City` VALUES (85, '320900', '盐城市', '320000');
INSERT INTO `Sys_City` VALUES (86, '321000', '扬州市', '320000');
INSERT INTO `Sys_City` VALUES (87, '321100', '镇江市', '320000');
INSERT INTO `Sys_City` VALUES (88, '321200', '泰州市', '320000');
INSERT INTO `Sys_City` VALUES (89, '321300', '宿迁市', '320000');
INSERT INTO `Sys_City` VALUES (90, '330100', '杭州市', '330000');
INSERT INTO `Sys_City` VALUES (91, '330200', '宁波市', '330000');
INSERT INTO `Sys_City` VALUES (92, '330300', '温州市', '330000');
INSERT INTO `Sys_City` VALUES (93, '330400', '嘉兴市', '330000');
INSERT INTO `Sys_City` VALUES (94, '330500', '湖州市', '330000');
INSERT INTO `Sys_City` VALUES (95, '330600', '绍兴市', '330000');
INSERT INTO `Sys_City` VALUES (96, '330700', '金华市', '330000');
INSERT INTO `Sys_City` VALUES (97, '330800', '衢州市', '330000');
INSERT INTO `Sys_City` VALUES (98, '330900', '舟山市', '330000');
INSERT INTO `Sys_City` VALUES (99, '331000', '台州市', '330000');
INSERT INTO `Sys_City` VALUES (100, '331100', '丽水市', '330000');
INSERT INTO `Sys_City` VALUES (101, '340100', '合肥市', '340000');
INSERT INTO `Sys_City` VALUES (102, '340200', '芜湖市', '340000');
INSERT INTO `Sys_City` VALUES (103, '340300', '蚌埠市', '340000');
INSERT INTO `Sys_City` VALUES (104, '340400', '淮南市', '340000');
INSERT INTO `Sys_City` VALUES (105, '340500', '马鞍山市', '340000');
INSERT INTO `Sys_City` VALUES (106, '340600', '淮北市', '340000');
INSERT INTO `Sys_City` VALUES (107, '340700', '铜陵市', '340000');
INSERT INTO `Sys_City` VALUES (108, '340800', '安庆市', '340000');
INSERT INTO `Sys_City` VALUES (109, '341000', '黄山市', '340000');
INSERT INTO `Sys_City` VALUES (110, '341100', '滁州市', '340000');
INSERT INTO `Sys_City` VALUES (111, '341200', '阜阳市', '340000');
INSERT INTO `Sys_City` VALUES (112, '341300', '宿州市', '340000');
INSERT INTO `Sys_City` VALUES (113, '341400', '巢湖市', '340000');
INSERT INTO `Sys_City` VALUES (114, '341500', '六安市', '340000');
INSERT INTO `Sys_City` VALUES (115, '341600', '亳州市', '340000');
INSERT INTO `Sys_City` VALUES (116, '341700', '池州市', '340000');
INSERT INTO `Sys_City` VALUES (117, '341800', '宣城市', '340000');
INSERT INTO `Sys_City` VALUES (118, '350100', '福州市', '350000');
INSERT INTO `Sys_City` VALUES (119, '350200', '厦门市', '350000');
INSERT INTO `Sys_City` VALUES (120, '350300', '莆田市', '350000');
INSERT INTO `Sys_City` VALUES (121, '350400', '三明市', '350000');
INSERT INTO `Sys_City` VALUES (122, '350500', '泉州市', '350000');
INSERT INTO `Sys_City` VALUES (123, '350600', '漳州市', '350000');
INSERT INTO `Sys_City` VALUES (124, '350700', '南平市', '350000');
INSERT INTO `Sys_City` VALUES (125, '350800', '龙岩市', '350000');
INSERT INTO `Sys_City` VALUES (126, '350900', '宁德市', '350000');
INSERT INTO `Sys_City` VALUES (127, '360100', '南昌市', '360000');
INSERT INTO `Sys_City` VALUES (128, '360200', '景德镇市', '360000');
INSERT INTO `Sys_City` VALUES (129, '360300', '萍乡市', '360000');
INSERT INTO `Sys_City` VALUES (130, '360400', '九江市', '360000');
INSERT INTO `Sys_City` VALUES (131, '360500', '新余市', '360000');
INSERT INTO `Sys_City` VALUES (132, '360600', '鹰潭市', '360000');
INSERT INTO `Sys_City` VALUES (133, '360700', '赣州市', '360000');
INSERT INTO `Sys_City` VALUES (134, '360800', '吉安市', '360000');
INSERT INTO `Sys_City` VALUES (135, '360900', '宜春市', '360000');
INSERT INTO `Sys_City` VALUES (136, '361000', '抚州市', '360000');
INSERT INTO `Sys_City` VALUES (137, '361100', '上饶市', '360000');
INSERT INTO `Sys_City` VALUES (138, '370100', '济南市', '370000');
INSERT INTO `Sys_City` VALUES (139, '370200', '青岛市', '370000');
INSERT INTO `Sys_City` VALUES (140, '370300', '淄博市', '370000');
INSERT INTO `Sys_City` VALUES (141, '370400', '枣庄市', '370000');
INSERT INTO `Sys_City` VALUES (142, '370500', '东营市', '370000');
INSERT INTO `Sys_City` VALUES (143, '370600', '烟台市', '370000');
INSERT INTO `Sys_City` VALUES (144, '370700', '潍坊市', '370000');
INSERT INTO `Sys_City` VALUES (145, '370800', '济宁市', '370000');
INSERT INTO `Sys_City` VALUES (146, '370900', '泰安市', '370000');
INSERT INTO `Sys_City` VALUES (147, '371000', '威海市', '370000');
INSERT INTO `Sys_City` VALUES (148, '371100', '日照市', '370000');
INSERT INTO `Sys_City` VALUES (149, '371200', '莱芜市', '370000');
INSERT INTO `Sys_City` VALUES (150, '371300', '临沂市', '370000');
INSERT INTO `Sys_City` VALUES (151, '371400', '德州市', '370000');
INSERT INTO `Sys_City` VALUES (152, '371500', '聊城市', '370000');
INSERT INTO `Sys_City` VALUES (153, '371600', '滨州市', '370000');
INSERT INTO `Sys_City` VALUES (154, '371700', '荷泽市', '370000');
INSERT INTO `Sys_City` VALUES (155, '410100', '郑州市', '410000');
INSERT INTO `Sys_City` VALUES (156, '410200', '开封市', '410000');
INSERT INTO `Sys_City` VALUES (157, '410300', '洛阳市', '410000');
INSERT INTO `Sys_City` VALUES (158, '410400', '平顶山市', '410000');
INSERT INTO `Sys_City` VALUES (159, '410500', '安阳市', '410000');
INSERT INTO `Sys_City` VALUES (160, '410600', '鹤壁市', '410000');
INSERT INTO `Sys_City` VALUES (161, '410700', '新乡市', '410000');
INSERT INTO `Sys_City` VALUES (162, '410800', '焦作市', '410000');
INSERT INTO `Sys_City` VALUES (163, '410900', '濮阳市', '410000');
INSERT INTO `Sys_City` VALUES (164, '411000', '许昌市', '410000');
INSERT INTO `Sys_City` VALUES (165, '411100', '漯河市', '410000');
INSERT INTO `Sys_City` VALUES (166, '411200', '三门峡市', '410000');
INSERT INTO `Sys_City` VALUES (167, '411300', '南阳市', '410000');
INSERT INTO `Sys_City` VALUES (168, '411400', '商丘市', '410000');
INSERT INTO `Sys_City` VALUES (169, '411500', '信阳市', '410000');
INSERT INTO `Sys_City` VALUES (170, '411600', '周口市', '410000');
INSERT INTO `Sys_City` VALUES (171, '411700', '驻马店市', '410000');
INSERT INTO `Sys_City` VALUES (172, '420100', '武汉市', '420000');
INSERT INTO `Sys_City` VALUES (173, '420200', '黄石市', '420000');
INSERT INTO `Sys_City` VALUES (174, '420300', '十堰市', '420000');
INSERT INTO `Sys_City` VALUES (175, '420500', '宜昌市', '420000');
INSERT INTO `Sys_City` VALUES (176, '420600', '襄樊市', '420000');
INSERT INTO `Sys_City` VALUES (177, '420700', '鄂州市', '420000');
INSERT INTO `Sys_City` VALUES (178, '420800', '荆门市', '420000');
INSERT INTO `Sys_City` VALUES (179, '420900', '孝感市', '420000');
INSERT INTO `Sys_City` VALUES (180, '421000', '荆州市', '420000');
INSERT INTO `Sys_City` VALUES (181, '421100', '黄冈市', '420000');
INSERT INTO `Sys_City` VALUES (182, '421200', '咸宁市', '420000');
INSERT INTO `Sys_City` VALUES (183, '421300', '随州市', '420000');
INSERT INTO `Sys_City` VALUES (184, '422800', '恩施土家族苗族自治州', '420000');
INSERT INTO `Sys_City` VALUES (185, '429000', '省直辖行政单位', '420000');
INSERT INTO `Sys_City` VALUES (186, '430100', '长沙市', '430000');
INSERT INTO `Sys_City` VALUES (187, '430200', '株洲市', '430000');
INSERT INTO `Sys_City` VALUES (188, '430300', '湘潭市', '430000');
INSERT INTO `Sys_City` VALUES (189, '430400', '衡阳市', '430000');
INSERT INTO `Sys_City` VALUES (190, '430500', '邵阳市', '430000');
INSERT INTO `Sys_City` VALUES (191, '430600', '岳阳市', '430000');
INSERT INTO `Sys_City` VALUES (192, '430700', '常德市', '430000');
INSERT INTO `Sys_City` VALUES (193, '430800', '张家界市', '430000');
INSERT INTO `Sys_City` VALUES (194, '430900', '益阳市', '430000');
INSERT INTO `Sys_City` VALUES (195, '431000', '郴州市', '430000');
INSERT INTO `Sys_City` VALUES (196, '431100', '永州市', '430000');
INSERT INTO `Sys_City` VALUES (197, '431200', '怀化市', '430000');
INSERT INTO `Sys_City` VALUES (198, '431300', '娄底市', '430000');
INSERT INTO `Sys_City` VALUES (199, '433100', '湘西土家族苗族自治州', '430000');
INSERT INTO `Sys_City` VALUES (200, '440100', '广州市', '440000');
INSERT INTO `Sys_City` VALUES (201, '440200', '韶关市', '440000');
INSERT INTO `Sys_City` VALUES (202, '440300', '深圳市', '440000');
INSERT INTO `Sys_City` VALUES (203, '440400', '珠海市', '440000');
INSERT INTO `Sys_City` VALUES (204, '440500', '汕头市', '440000');
INSERT INTO `Sys_City` VALUES (205, '440600', '佛山市', '440000');
INSERT INTO `Sys_City` VALUES (206, '440700', '江门市', '440000');
INSERT INTO `Sys_City` VALUES (207, '440800', '湛江市', '440000');
INSERT INTO `Sys_City` VALUES (208, '440900', '茂名市', '440000');
INSERT INTO `Sys_City` VALUES (209, '441200', '肇庆市', '440000');
INSERT INTO `Sys_City` VALUES (210, '441300', '惠州市', '440000');
INSERT INTO `Sys_City` VALUES (211, '441400', '梅州市', '440000');
INSERT INTO `Sys_City` VALUES (212, '441500', '汕尾市', '440000');
INSERT INTO `Sys_City` VALUES (213, '441600', '河源市', '440000');
INSERT INTO `Sys_City` VALUES (214, '441700', '阳江市', '440000');
INSERT INTO `Sys_City` VALUES (215, '441800', '清远市', '440000');
INSERT INTO `Sys_City` VALUES (216, '441900', '东莞市', '440000');
INSERT INTO `Sys_City` VALUES (217, '442000', '中山市', '440000');
INSERT INTO `Sys_City` VALUES (218, '445100', '潮州市', '440000');
INSERT INTO `Sys_City` VALUES (219, '445200', '揭阳市', '440000');
INSERT INTO `Sys_City` VALUES (220, '445300', '云浮市', '440000');
INSERT INTO `Sys_City` VALUES (221, '450100', '南宁市', '450000');
INSERT INTO `Sys_City` VALUES (222, '450200', '柳州市', '450000');
INSERT INTO `Sys_City` VALUES (223, '450300', '桂林市', '450000');
INSERT INTO `Sys_City` VALUES (224, '450400', '梧州市', '450000');
INSERT INTO `Sys_City` VALUES (225, '450500', '北海市', '450000');
INSERT INTO `Sys_City` VALUES (226, '450600', '防城港市', '450000');
INSERT INTO `Sys_City` VALUES (227, '450700', '钦州市', '450000');
INSERT INTO `Sys_City` VALUES (228, '450800', '贵港市', '450000');
INSERT INTO `Sys_City` VALUES (229, '450900', '玉林市', '450000');
INSERT INTO `Sys_City` VALUES (230, '451000', '百色市', '450000');
INSERT INTO `Sys_City` VALUES (231, '451100', '贺州市', '450000');
INSERT INTO `Sys_City` VALUES (232, '451200', '河池市', '450000');
INSERT INTO `Sys_City` VALUES (233, '451300', '来宾市', '450000');
INSERT INTO `Sys_City` VALUES (234, '451400', '崇左市', '450000');
INSERT INTO `Sys_City` VALUES (235, '460100', '海口市', '460000');
INSERT INTO `Sys_City` VALUES (236, '460200', '三亚市', '460000');
INSERT INTO `Sys_City` VALUES (237, '469000', '省直辖县级行政单位', '460000');
INSERT INTO `Sys_City` VALUES (238, '500100', '市辖区', '500000');
INSERT INTO `Sys_City` VALUES (239, '500200', '县', '500000');
INSERT INTO `Sys_City` VALUES (240, '500300', '市', '500000');
INSERT INTO `Sys_City` VALUES (241, '510100', '成都市', '510000');
INSERT INTO `Sys_City` VALUES (242, '510300', '自贡市', '510000');
INSERT INTO `Sys_City` VALUES (243, '510400', '攀枝花市', '510000');
INSERT INTO `Sys_City` VALUES (244, '510500', '泸州市', '510000');
INSERT INTO `Sys_City` VALUES (245, '510600', '德阳市', '510000');
INSERT INTO `Sys_City` VALUES (246, '510700', '绵阳市', '510000');
INSERT INTO `Sys_City` VALUES (247, '510800', '广元市', '510000');
INSERT INTO `Sys_City` VALUES (248, '510900', '遂宁市', '510000');
INSERT INTO `Sys_City` VALUES (249, '511000', '内江市', '510000');
INSERT INTO `Sys_City` VALUES (250, '511100', '乐山市', '510000');
INSERT INTO `Sys_City` VALUES (251, '511300', '南充市', '510000');
INSERT INTO `Sys_City` VALUES (252, '511400', '眉山市', '510000');
INSERT INTO `Sys_City` VALUES (253, '511500', '宜宾市', '510000');
INSERT INTO `Sys_City` VALUES (254, '511600', '广安市', '510000');
INSERT INTO `Sys_City` VALUES (255, '511700', '达州市', '510000');
INSERT INTO `Sys_City` VALUES (256, '511800', '雅安市', '510000');
INSERT INTO `Sys_City` VALUES (257, '511900', '巴中市', '510000');
INSERT INTO `Sys_City` VALUES (258, '512000', '资阳市', '510000');
INSERT INTO `Sys_City` VALUES (259, '513200', '阿坝藏族羌族自治州', '510000');
INSERT INTO `Sys_City` VALUES (260, '513300', '甘孜藏族自治州', '510000');
INSERT INTO `Sys_City` VALUES (261, '513400', '凉山彝族自治州', '510000');
INSERT INTO `Sys_City` VALUES (262, '520100', '贵阳市', '520000');
INSERT INTO `Sys_City` VALUES (263, '520200', '六盘水市', '520000');
INSERT INTO `Sys_City` VALUES (264, '520300', '遵义市', '520000');
INSERT INTO `Sys_City` VALUES (265, '520400', '安顺市', '520000');
INSERT INTO `Sys_City` VALUES (266, '522200', '铜仁地区', '520000');
INSERT INTO `Sys_City` VALUES (267, '522300', '黔西南布依族苗族自治州', '520000');
INSERT INTO `Sys_City` VALUES (268, '522400', '毕节地区', '520000');
INSERT INTO `Sys_City` VALUES (269, '522600', '黔东南苗族侗族自治州', '520000');
INSERT INTO `Sys_City` VALUES (270, '522700', '黔南布依族苗族自治州', '520000');
INSERT INTO `Sys_City` VALUES (271, '530100', '昆明市', '530000');
INSERT INTO `Sys_City` VALUES (272, '530300', '曲靖市', '530000');
INSERT INTO `Sys_City` VALUES (273, '530400', '玉溪市', '530000');
INSERT INTO `Sys_City` VALUES (274, '530500', '保山市', '530000');
INSERT INTO `Sys_City` VALUES (275, '530600', '昭通市', '530000');
INSERT INTO `Sys_City` VALUES (276, '530700', '丽江市', '530000');
INSERT INTO `Sys_City` VALUES (277, '530800', '思茅市', '530000');
INSERT INTO `Sys_City` VALUES (278, '530900', '临沧市', '530000');
INSERT INTO `Sys_City` VALUES (279, '532300', '楚雄彝族自治州', '530000');
INSERT INTO `Sys_City` VALUES (280, '532500', '红河哈尼族彝族自治州', '530000');
INSERT INTO `Sys_City` VALUES (281, '532600', '文山壮族苗族自治州', '530000');
INSERT INTO `Sys_City` VALUES (282, '532800', '西双版纳傣族自治州', '530000');
INSERT INTO `Sys_City` VALUES (283, '532900', '大理白族自治州', '530000');
INSERT INTO `Sys_City` VALUES (284, '533100', '德宏傣族景颇族自治州', '530000');
INSERT INTO `Sys_City` VALUES (285, '533300', '怒江傈僳族自治州', '530000');
INSERT INTO `Sys_City` VALUES (286, '533400', '迪庆藏族自治州', '530000');
INSERT INTO `Sys_City` VALUES (287, '540100', '拉萨市', '540000');
INSERT INTO `Sys_City` VALUES (288, '542100', '昌都地区', '540000');
INSERT INTO `Sys_City` VALUES (289, '542200', '山南地区', '540000');
INSERT INTO `Sys_City` VALUES (290, '542300', '日喀则地区', '540000');
INSERT INTO `Sys_City` VALUES (291, '542400', '那曲地区', '540000');
INSERT INTO `Sys_City` VALUES (292, '542500', '阿里地区', '540000');
INSERT INTO `Sys_City` VALUES (293, '542600', '林芝地区', '540000');
INSERT INTO `Sys_City` VALUES (294, '610100', '西安市', '610000');
INSERT INTO `Sys_City` VALUES (295, '610200', '铜川市', '610000');
INSERT INTO `Sys_City` VALUES (296, '610300', '宝鸡市', '610000');
INSERT INTO `Sys_City` VALUES (297, '610400', '咸阳市', '610000');
INSERT INTO `Sys_City` VALUES (298, '610500', '渭南市', '610000');
INSERT INTO `Sys_City` VALUES (299, '610600', '延安市', '610000');
INSERT INTO `Sys_City` VALUES (300, '610700', '汉中市', '610000');
INSERT INTO `Sys_City` VALUES (301, '610800', '榆林市', '610000');
INSERT INTO `Sys_City` VALUES (302, '610900', '安康市', '610000');
INSERT INTO `Sys_City` VALUES (303, '611000', '商洛市', '610000');
INSERT INTO `Sys_City` VALUES (304, '620100', '兰州市', '620000');
INSERT INTO `Sys_City` VALUES (305, '620200', '嘉峪关市', '620000');
INSERT INTO `Sys_City` VALUES (306, '620300', '金昌市', '620000');
INSERT INTO `Sys_City` VALUES (307, '620400', '白银市', '620000');
INSERT INTO `Sys_City` VALUES (308, '620500', '天水市', '620000');
INSERT INTO `Sys_City` VALUES (309, '620600', '武威市', '620000');
INSERT INTO `Sys_City` VALUES (310, '620700', '张掖市', '620000');
INSERT INTO `Sys_City` VALUES (311, '620800', '平凉市', '620000');
INSERT INTO `Sys_City` VALUES (312, '620900', '酒泉市', '620000');
INSERT INTO `Sys_City` VALUES (313, '621000', '庆阳市', '620000');
INSERT INTO `Sys_City` VALUES (314, '621100', '定西市', '620000');
INSERT INTO `Sys_City` VALUES (315, '621200', '陇南市', '620000');
INSERT INTO `Sys_City` VALUES (316, '622900', '临夏回族自治州', '620000');
INSERT INTO `Sys_City` VALUES (317, '623000', '甘南藏族自治州', '620000');
INSERT INTO `Sys_City` VALUES (318, '630100', '西宁市', '630000');
INSERT INTO `Sys_City` VALUES (319, '632100', '海东地区', '630000');
INSERT INTO `Sys_City` VALUES (320, '632200', '海北藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (321, '632300', '黄南藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (322, '632500', '海南藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (323, '632600', '果洛藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (324, '632700', '玉树藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (325, '632800', '海西蒙古族藏族自治州', '630000');
INSERT INTO `Sys_City` VALUES (326, '640100', '银川市', '640000');
INSERT INTO `Sys_City` VALUES (327, '640200', '石嘴山市', '640000');
INSERT INTO `Sys_City` VALUES (328, '640300', '吴忠市', '640000');
INSERT INTO `Sys_City` VALUES (329, '640400', '固原市', '640000');
INSERT INTO `Sys_City` VALUES (330, '640500', '中卫市', '640000');
INSERT INTO `Sys_City` VALUES (331, '650100', '乌鲁木齐市', '650000');
INSERT INTO `Sys_City` VALUES (332, '650200', '克拉玛依市', '650000');
INSERT INTO `Sys_City` VALUES (333, '652100', '吐鲁番地区', '650000');
INSERT INTO `Sys_City` VALUES (334, '652200', '哈密地区', '650000');
INSERT INTO `Sys_City` VALUES (335, '652300', '昌吉回族自治州', '650000');
INSERT INTO `Sys_City` VALUES (336, '652700', '博尔塔拉蒙古自治州', '650000');
INSERT INTO `Sys_City` VALUES (337, '652800', '巴音郭楞蒙古自治州', '650000');
INSERT INTO `Sys_City` VALUES (338, '652900', '阿克苏地区', '650000');
INSERT INTO `Sys_City` VALUES (339, '653000', '克孜勒苏柯尔克孜自治州', '650000');
INSERT INTO `Sys_City` VALUES (340, '653100', '喀什地区', '650000');
INSERT INTO `Sys_City` VALUES (341, '653200', '和田地区', '650000');
INSERT INTO `Sys_City` VALUES (342, '654000', '伊犁哈萨克自治州', '650000');
INSERT INTO `Sys_City` VALUES (343, '654200', '塔城地区', '650000');
INSERT INTO `Sys_City` VALUES (344, '654300', '阿勒泰地区', '650000');
INSERT INTO `Sys_City` VALUES (345, '659000', '省直辖行政单位', '650000');

-- ----------------------------
-- Table structure for Sys_Dictionary
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Dictionary`;
CREATE TABLE `Sys_Dictionary`  (
  `Dic_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Config` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DBServer` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DbSql` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DicName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DicNo` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `ParentId` int(11) NULL DEFAULT NULL,
  `Remark` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`Dic_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Dictionary
-- ----------------------------
INSERT INTO `Sys_Dictionary` VALUES (3, '{valueField: \'Enable\',\ntextField: \'Enable\',\n containField: null,\n  handler: null }', '2019-07-05 17:36:23.000000', NULL, 'admin', '1', NULL, '是否值', 'enable', 1, '超级管理员', '2020-01-05 18:58:01.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (30, '{valueField: \'Success\',\n textField: \'Success\', \n containField: null,\n handler: null }\n', '2018-06-11 18:26:05.000000', 0, '测试超级管理员', NULL, NULL, '响应状态', 'restatus', 1, '测试超级管理员', '2018-06-12 10:21:48.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (31, '{valueField: \'LogType\',\n textField: \'LogType\', \n containField: null,\n handler: null }\n', '2018-06-12 14:46:07.000000', NULL, '测试超级管理员', NULL, NULL, '日志类型', 'log', 1, '超级管理员', '2023-02-22 00:31:10.435088', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (32, '{valueField: \'Role_Id\',\n textField: \'RoleName\', \n containField: [\'Role_Id\',\'RoleName\'],\n handler: null }\n', '2018-06-14 16:48:35.000000', NULL, '测试超级管理员', NULL, 'SELECT Role_Id as \'key\',RoleName as \'value\' FROM Sys_Role WHERE Enable=1\n', '角色列表', 'roles', 1, '测试超级管理员', '2018-07-13 15:03:53.000000', 1, 123, 0, 'sql语句需要key,value列，界面才能绑定数据源');
INSERT INTO `Sys_Dictionary` VALUES (35, '{\n valueField: \'AuditStatus\',\n textField: \'AuditStatus\',\n  containField:null \n}', '2018-07-10 10:51:37.000000', NULL, '测试超级管理员', NULL, NULL, '审核状态', 'audit', 1, '测试超级管理员', '2018-07-10 11:02:59.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (38, '{\n valueField: \'City\',\n textField: \'City\',\n  containField:null \n}', '2018-07-10 14:18:25.000000', NULL, '测试超级管理员', NULL, '\nSELECT  CASE WHEN  CityName=\'市辖区\' THEN  ProvinceName ELSE CityName end  as  \'key\',CASE WHEN  CityName=\'市辖区\' THEN  ProvinceName ELSE CityName end  as  \'value\'  FROM Sys_City AS a \nINNER JOIN Sys_Province AS b \nON a.ProvinceCode=b.ProvinceCode\nWHERE a.CityName<> \'县\'', '城市', 'city', 1, '超级管理员', '2020-02-01 22:27:08.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (46, '{\n valueField: \'ProvinceName\',\n textField: \'ProvinceName\',\n  containField:null \n}', '2018-07-16 13:27:34.000000', NULL, '测试超级管理员', NULL, '\nSELECT  CASE WHEN  CityName=\'市辖区\' THEN  ProvinceName ELSE CityName end  as  \'key\',CASE WHEN  CityName=\'市辖区\' THEN  ProvinceName ELSE CityName end  as  \'value\'  FROM Sys_City AS a \nINNER JOIN Sys_Province AS b \nON a.ProvinceCode=b.ProvinceCode\nWHERE a.CityName<> \'县\'', '省列表', 'pro', 1, '超级管理员', '2020-02-01 22:26:59.000000', 1, NULL, 0, 'sql语句需要key,value列，界面才能绑定数据源');
INSERT INTO `Sys_Dictionary` VALUES (49, '{\n valueField: \'Gender\',\n textField: \'Gender\',\n  containField:null \n}', '2018-07-23 10:04:45.000000', NULL, '测试超级管理员', NULL, NULL, '性别', 'gender', 1, '测试超级管理员', '2018-07-23 11:10:28.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (50, '{\n valueField: \'Enable\',\n textField: \'Enable\',\n  containField:null \n}', '2018-07-23 15:36:43.000000', 1, '测试超级管理员', NULL, NULL, '启用状态', 'status', 1, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (59, '{\n valueField: \'IsRegregisterPhone\',\n textField: \'IsRegregisterPhone\',\n  containField:null \n}', '2018-08-29 15:54:21.000000', 1, '测试超级管理员', NULL, NULL, '是否手机用户', 'isphone', 1, '超级管理员', '2019-11-01 10:05:12.000000', 1, NULL, 0, NULL);
INSERT INTO `Sys_Dictionary` VALUES (66, NULL, '2020-12-29 21:50:16.000000', 1, '超级管理员', NULL, 'SELECT Role_Id AS id,parentId,Role_Id AS [key],RoleName AS value FROM Sys_Role\n', '级联角色', 'tree_roles', 1, NULL, NULL, NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for Sys_DictionaryList
-- ----------------------------
DROP TABLE IF EXISTS `Sys_DictionaryList`;
CREATE TABLE `Sys_DictionaryList`  (
  `DicList_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DicName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DicValue` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Dic_ID` int(11) NULL DEFAULT NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `Remark` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`DicList_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 423 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_DictionaryList
-- ----------------------------
INSERT INTO `Sys_DictionaryList` VALUES (3, NULL, 1, 'admin', '否', '0', 3, NULL, '超级管理员', '2020-01-05 18:58:01.000000', 1, 2, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (4, NULL, 1, 'xxx', '是', '1', 3, NULL, '超级管理员', '2020-01-05 18:58:01.000000', 1, 1, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (22, '2018-06-11 18:26:05.000000', 1, '测试超级管理员', '其他', '0', 30, NULL, '超级管理员', '2019-08-21 16:49:43.000000', 1, 10, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (23, '2018-06-11 18:26:05.000000', 1, '测试超级管理员', '成功', '1', 30, NULL, '超级管理员', '2019-08-21 16:49:43.000000', 1, 100, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (24, '2018-06-12 09:41:58.000000', 1, '测试超级管理员', '异常', '2', 30, NULL, '超级管理员', '2019-08-21 16:49:43.000000', 1, 50, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (25, '2018-06-12 14:46:08.000000', 1, '测试超级管理员', '系统', 'System', 31, NULL, '超级管理员', '2023-02-22 00:31:10.484763', 1, 100, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (26, '2018-06-12 14:47:32.000000', 1, '测试超级管理员', '登陆', 'Login', 31, NULL, '超级管理员', '2023-02-22 00:31:10.484722', 1, 90, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (27, '2018-06-15 15:29:58.000000', 1, '测试超级管理员', '新建', 'Add', 31, NULL, '超级管理员', '2023-02-22 00:31:10.484676', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (28, '2018-06-15 15:29:58.000000', 1, '测试超级管理员', '删除', 'Del', 31, 1, '超级管理员', '2023-02-22 00:31:10.484632', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (29, '2018-06-15 15:30:34.000000', 1, '测试超级管理员', '编辑', 'Edit', 31, 1, '超级管理员', '2023-02-22 00:31:10.484584', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (37, '2018-07-10 10:51:38.000000', 1, '测试超级管理员', '审核中', '0', 35, NULL, '测试超级管理员', '2018-07-10 11:02:59.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (38, '2018-07-10 10:51:38.000000', 1, '测试超级管理员', '审核通过', '1', 35, NULL, '测试超级管理员', '2018-07-10 11:02:59.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (39, '2018-07-10 10:51:38.000000', 1, '测试超级管理员', '审核未通过', '2', 35, NULL, '测试超级管理员', '2018-07-10 11:02:59.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (68, '2018-07-11 13:20:11.000000', 1, '测试超级管理员', 'App登陆', 'ApiLogin', 31, NULL, '超级管理员', '2023-02-22 00:31:10.484529', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (69, '2018-07-11 15:57:03.000000', 1, '测试超级管理员', 'App发送验证码', 'ApiSendPIN', 31, NULL, '超级管理员', '2023-02-22 00:31:10.484413', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (71, '2018-07-11 17:11:19.000000', 1, '测试超级管理员', 'PC请求异常', 'Exception', 31, NULL, '超级管理员', '2023-02-22 00:31:10.477207', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (90, '2018-07-23 10:04:45.000000', 1, '测试超级管理员', '男', '0', 49, NULL, '测试超级管理员', '2018-07-23 11:10:28.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (91, '2018-07-23 10:04:45.000000', 1, '测试超级管理员', '女', '1', 49, NULL, '测试超级管理员', '2018-07-23 11:10:28.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (96, '2018-07-23 15:36:43.000000', 1, '测试超级管理员', '未启用', '0', 50, 1, '超级管理员', '2019-08-16 18:17:47.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (97, '2018-07-23 15:36:43.000000', 1, '测试超级管理员', '已启用', '1', 50, 1, '超级管理员', '2019-08-16 18:17:47.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (98, '2018-07-23 15:36:43.000000', 1, '测试超级管理员', '已删除', '2', 50, 1, '超级管理员', '2019-08-16 18:17:47.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (128, '2018-08-29 15:54:21.000000', 1, '测试超级管理员', '是', '1', 59, 0, '超级管理员', '2019-11-01 10:05:12.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (129, '2018-08-29 15:54:21.000000', 1, '测试超级管理员', '否', '0', 59, 1, '超级管理员', '2019-11-01 10:05:12.000000', 1, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (145, '2019-08-21 16:49:43.000000', 1, '超级管理员', 'Info', '3', 30, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `Sys_DictionaryList` VALUES (422, '2020-01-05 18:58:01.000000', 1, '超级管理员', 'xx11', '2', 3, 0, NULL, NULL, NULL, 2, NULL);

-- ----------------------------
-- Table structure for Sys_Log
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Log`;
CREATE TABLE `Sys_Log`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `BeginDate` timestamp(6) NULL DEFAULT NULL,
  `BrowserType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ElapsedTime` int(11) NULL DEFAULT NULL,
  `EndDate` timestamp(6) NULL DEFAULT NULL,
  `ExceptionInfo` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `LogType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `RequestParameter` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ResponseParameter` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Role_Id` int(11) NULL DEFAULT NULL,
  `ServiceIP` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Success` int(11) NULL DEFAULT NULL,
  `Url` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UserIP` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UserName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `User_Id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Log
-- ----------------------------
INSERT INTO `Sys_Log` VALUES (1, '2023-02-22 00:50:34.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 32, '2023-02-22 00:50:34.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/Sys_Dictionary/GetVueDictionary', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (2, '2023-02-22 00:50:34.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 90, '2023-02-22 00:50:34.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/Sys_User/getPageData', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (3, '2023-02-22 00:50:34.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:34.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/201912141113355553/07.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (4, '2023-02-22 00:50:34.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:34.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/202004271001535915/04.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (8, '2023-02-22 00:50:36.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:36.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/202004271001535915/04.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (9, '2023-02-22 00:50:42.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 156, '2023-02-22 00:50:42.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/user/modifyUserPwd', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (10, '2023-02-22 00:50:48.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 87, '2023-02-22 00:50:48.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/getTreeItem', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (11, '2023-02-22 00:50:49.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 26, '2023-02-22 00:50:49.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/getTreeItem', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (13, '2023-02-22 00:50:52.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 138, '2023-02-22 00:50:52.000000', '', 'Info', '表:Sys_User,菜单：用户管理,权限[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导入\",\"value\":\"Import\"},{\"text\":\"导出\",\"value\":\"Export\"},{\"text\":\"上传\",\"value\":\"Upload\"}],成功保存成功', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/save', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (14, '2023-02-22 00:50:52.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 138, '2023-02-22 00:50:52.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/save', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (16, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:55.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/202004271001535915/04.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (17, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 40, '2023-02-22 00:50:55.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/getTreeMenu', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (18, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 67, '2023-02-22 00:50:55.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/Sys_Dictionary/GetVueDictionary', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (19, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 104, '2023-02-22 00:50:55.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/Sys_User/getPageData', '127.0.0.1', '超级管理员', 1);
INSERT INTO `Sys_Log` VALUES (20, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:55.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/201912141113355553/07.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (21, '2023-02-22 00:50:55.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:50:55.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/202004271001535915/04.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (23, '2023-02-22 00:51:02.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 30, '2023-02-22 00:51:02.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/User/getVierificationCode', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (24, '2023-02-22 00:51:10.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 89, '2023-02-22 00:51:10.000000', '', 'Login', '{\"UserName\":\"admin\",\"Password\":\"\",\"VerificationCode\":\"rgjc\",\"UUID\":\"0070d92d-5ae1-4954-823c-7a97f3c3d911\"}', '登陆成功', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/user/login', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (25, '2023-02-22 00:51:10.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 91, '2023-02-22 00:51:11.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/user/login', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (26, '2023-02-22 00:51:11.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 0, '2023-02-22 00:51:11.000000', '', 'System', '', '', 0, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/Upload/Tables/Sys_User/202004271001535915/04.jpg', '127.0.0.1', '', 0);
INSERT INTO `Sys_Log` VALUES (27, '2023-02-22 00:51:11.000000', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 29, '2023-02-22 00:51:11.000000', '', 'System', '', '', 1, '127.0.0.1:9991', 3, 'http://127.0.0.1:9991/api/menu/getTreeMenu', '127.0.0.1', '超级管理员', 1);

-- ----------------------------
-- Table structure for Sys_Menu
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Menu`;
CREATE TABLE `Sys_Menu`  (
  `Menu_Id` int(11) NOT NULL AUTO_INCREMENT,
  `MenuName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Auth` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Icon` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Description` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `TableName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ParentId` int(11) NULL DEFAULT NULL,
  `Url` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `MenuType` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Menu_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Menu
-- ----------------------------
INSERT INTO `Sys_Menu` VALUES (2, '用户管理', '[{\"text\":\"查询\",\"value\":\"Search\"}]', 'el-icon-user', NULL, 1, 1600, '.', 0, NULL, '2017-08-28 12:21:13.000000', '2017-08-28 11:12:45', '2022-04-22 20:02:36.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (3, '角色管理', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导出\",\"value\":\"Export\"}]', NULL, NULL, 1, 900, 'Sys_Role', 2, '/Sys_Role', '2017-09-12 16:20:02.000000', '2017-08-28 14:19:13', '2020-12-29 21:48:16.000000', '超级管理员', NULL);
INSERT INTO `Sys_Menu` VALUES (9, '用户管理', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导入\",\"value\":\"Import\"},{\"text\":\"导出\",\"value\":\"Export\"},{\"text\":\"上传\",\"value\":\"Upload\"}]', NULL, NULL, 1, 11110, 'Sys_User', 2, '/Sys_User', NULL, NULL, '2023-02-22 00:50:52.166375', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (61, '系统设置', '[{\"text\":\"查询\",\"value\":\"Search\"}]', 'el-icon-setting', NULL, 1, 1000, '系统', 0, '/', '2019-07-12 14:04:04.000000', '超级管理员', '2022-04-22 20:03:03.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (62, '菜单设置', '[{\"text\":\"查询\",\"value\":\"Search\"}]', NULL, NULL, 1, 10, 'Sys_Menu', 61, '/sysmenu', '2019-07-12 14:04:35.000000', '超级管理员', '2019-10-24 12:00:38.000000', '超级管理员', NULL);
INSERT INTO `Sys_Menu` VALUES (63, '下拉框绑定设置', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导出\",\"value\":\"Export\"}]', NULL, NULL, 1, 10, 'Sys_Dictionary', 61, '/Sys_Dictionary', '2019-07-12 14:05:58.000000', '超级管理员', '2019-08-16 17:41:15.000000', '超级管理员', NULL);
INSERT INTO `Sys_Menu` VALUES (64, '代码生成', '[{\"text\":\"查询\",\"value\":\"Search\"}]', 'el-icon-document', NULL, 1, 1500, '代码在线生成器', 0, '/coding', '2019-07-12 14:07:55.000000', '超级管理员', '2022-04-22 20:02:56.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (65, '代码生成', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"}]', NULL, NULL, 1, 10, '/', 64, '/coder', '2019-07-12 14:08:58.000000', '超级管理员', '2022-04-22 20:02:27.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (71, 'vue权限管理', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"编辑\",\"value\":\"Update\"}]', 'ivu-icon ivu-icon-ios-boat', NULL, 1, 1000, 'Sys_Role1', 2, '/permission', '2019-08-10 10:25:36.000000', '超级管理员', NULL, NULL, NULL);
INSERT INTO `Sys_Menu` VALUES (95, '放此节点下', NULL, NULL, NULL, 1, 1, '/', 45, NULL, '2020-05-05 13:20:30.000000', '超级管理员', '2019-09-20 09:54:26.000000', NULL, NULL);
INSERT INTO `Sys_Menu` VALUES (96, '日志管理', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"导出\",\"value\":\"Export\"}]', 'el-icon-set-up', NULL, 1, 0, '.', 0, NULL, '2020-11-08 21:55:27.000000', '超级管理员', '2022-04-22 20:03:33.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (97, '日志管理', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"导出\",\"value\":\"Export\"}]', NULL, NULL, 1, 0, 'Sys_Log', 96, '/Sys_Log', '2020-11-08 21:56:01.000000', '超级管理员', '2020-11-08 21:57:03.000000', '超级管理员', NULL);
INSERT INTO `Sys_Menu` VALUES (98, '业务库', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导入\",\"value\":\"Import\"},{\"text\":\"导出\",\"value\":\"Export\"}]', NULL, NULL, 1, 9000, 'ServiceDbTest', 0, '/ServiceDbTest', '2022-05-04 11:35:42.000000', '超级管理员', '2022-05-04 11:38:46.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (99, '报表库', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导入\",\"value\":\"Import\"},{\"text\":\"导出\",\"value\":\"Export\"}]', NULL, NULL, 1, 8000, 'ReportDBTest', 0, '/ReportDBTest', '2022-05-04 11:36:13.000000', '超级管理员', '2023-02-22 00:37:09.118054', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (100, '消息推送', '[{\"text\":\"查询\",\"value\":\"Search\"}]', NULL, NULL, 1, 8000, '.', 0, '/signalR', '2022-05-04 11:44:22.000000', '超级管理员', '2022-05-04 11:44:30.000000', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (101, '定时任务', '[{\"text\":\"查询\",\"value\":\"Search\"}]', '', NULL, 1, 2000, '定时任务', 0, '', '2023-02-22 00:46:28.524285', '超级管理员', NULL, NULL, 0);
INSERT INTO `Sys_Menu` VALUES (102, '任务配置', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导出\",\"value\":\"Export\"}]', '', NULL, 1, 100, 'Sys_QuartzOptions', 101, '/Sys_QuartzOptions', '2023-02-22 00:47:04.054363', '超级管理员', NULL, NULL, 0);
INSERT INTO `Sys_Menu` VALUES (103, '执行记录', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"导出\",\"value\":\"Export\"}]', '', NULL, 1, 100, 'Sys_QuartzLog', 101, '/Sys_QuartzLog', '2023-02-22 00:47:21.146542', '超级管理员', NULL, NULL, 0);
INSERT INTO `Sys_Menu` VALUES (104, '流程管理', '[{\"text\":\"查询\",\"value\":\"Search\"}]', '', NULL, 1, 1900, '流程管理', 0, '', '2023-02-22 00:47:36.102276', '超级管理员', NULL, NULL, 0);
INSERT INTO `Sys_Menu` VALUES (105, '审批流程', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"新建\",\"value\":\"Add\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"编辑\",\"value\":\"Update\"},{\"text\":\"导出\",\"value\":\"Export\"}]', '', NULL, 1, 200, 'Sys_WorkFlow', 104, '/Sys_WorkFlow', '2023-02-22 00:47:55.393748', '超级管理员', NULL, NULL, 0);
INSERT INTO `Sys_Menu` VALUES (106, '我的任务', '[{\"text\":\"查询\",\"value\":\"Search\"},{\"text\":\"删除\",\"value\":\"Delete\"},{\"text\":\"导出\",\"value\":\"Export\"}]', '', NULL, 1, 100, 'Sys_WorkFlowTable', 104, '/Sys_WorkFlowTable', '2023-02-22 00:48:18.220950', '超级管理员', '2023-02-22 00:49:18.751034', '超级管理员', 0);
INSERT INTO `Sys_Menu` VALUES (107, '发起流程', '[{\"text\":\"查询\",\"value\":\"Search\"}]', '', NULL, 1, 50, '发起流程', 104, '/flowdemo', '2023-02-22 00:48:34.733003', '超级管理员', '2023-02-22 00:49:21.070302', '超级管理员', 0);

-- ----------------------------
-- Table structure for Sys_Province
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Province`;
CREATE TABLE `Sys_Province`  (
  `ProvinceId` int(11) NULL DEFAULT NULL,
  `ProvinceCode` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ProvinceName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `RegionCode` text CHARACTER SET utf8 COLLATE utf8_bin NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Province
-- ----------------------------
INSERT INTO `Sys_Province` VALUES (1, '110000', '北京市', '华北');
INSERT INTO `Sys_Province` VALUES (2, '120000', '天津市', '华北');
INSERT INTO `Sys_Province` VALUES (3, '130000', '河北省', '华北');
INSERT INTO `Sys_Province` VALUES (4, '140000', '山西省', '华北');
INSERT INTO `Sys_Province` VALUES (5, '150000', '内蒙古自治区', '华北');
INSERT INTO `Sys_Province` VALUES (6, '210000', '辽宁省', '东北');
INSERT INTO `Sys_Province` VALUES (7, '220000', '吉林省', '东北');
INSERT INTO `Sys_Province` VALUES (8, '230000', '黑龙江省', '东北');
INSERT INTO `Sys_Province` VALUES (9, '310000', '上海市', '华东');
INSERT INTO `Sys_Province` VALUES (10, '320000', '江苏省', '华东');
INSERT INTO `Sys_Province` VALUES (11, '330000', '浙江省', '华东');
INSERT INTO `Sys_Province` VALUES (12, '340000', '安徽省', '华东');
INSERT INTO `Sys_Province` VALUES (13, '350000', '福建省', '华东');
INSERT INTO `Sys_Province` VALUES (14, '360000', '江西省', '华东');
INSERT INTO `Sys_Province` VALUES (15, '370000', '山东省', '华东');
INSERT INTO `Sys_Province` VALUES (16, '410000', '河南省', '华中');
INSERT INTO `Sys_Province` VALUES (17, '420000', '湖北省', '华中');
INSERT INTO `Sys_Province` VALUES (18, '430000', '湖南省', '华中');
INSERT INTO `Sys_Province` VALUES (19, '440000', '广东省', '华南');
INSERT INTO `Sys_Province` VALUES (20, '450000', '广西壮族自治区', '华南');
INSERT INTO `Sys_Province` VALUES (21, '460000', '海南省', '华南');
INSERT INTO `Sys_Province` VALUES (22, '500000', '重庆市', '西南');
INSERT INTO `Sys_Province` VALUES (23, '510000', '四川省', '西南');
INSERT INTO `Sys_Province` VALUES (24, '520000', '贵州省', '西南');
INSERT INTO `Sys_Province` VALUES (25, '530000', '云南省', '西南');
INSERT INTO `Sys_Province` VALUES (26, '540000', '西藏自治区', '西南');
INSERT INTO `Sys_Province` VALUES (27, '610000', '陕西省', '西北');
INSERT INTO `Sys_Province` VALUES (28, '620000', '甘肃省', '西北');
INSERT INTO `Sys_Province` VALUES (29, '630000', '青海省', '西北');
INSERT INTO `Sys_Province` VALUES (30, '640000', '宁夏回族自治区', '西北');
INSERT INTO `Sys_Province` VALUES (31, '650000', '新疆维吾尔自治区', '西北');
INSERT INTO `Sys_Province` VALUES (32, '710000', '台湾省', '港澳台');
INSERT INTO `Sys_Province` VALUES (33, '810000', '香港特别行政区', '港澳台');
INSERT INTO `Sys_Province` VALUES (34, '820000', '澳门特别行政区', '港澳台');
INSERT INTO `Sys_Province` VALUES (35, 'thd', '桃花岛', '东北');
INSERT INTO `Sys_Province` VALUES (43, '测试1', '测试1', '港澳台');

-- ----------------------------
-- Table structure for Sys_QuartzLog
-- ----------------------------
DROP TABLE IF EXISTS `Sys_QuartzLog`;
CREATE TABLE `Sys_QuartzLog`  (
  `LogId` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TaskName` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `ElapsedTime` int(11) NULL DEFAULT NULL COMMENT '耗时(秒)',
  `StratDate` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `EndDate` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `Result` int(11) NULL DEFAULT NULL COMMENT '是否成功',
  `ResponseContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回内容',
  `ErrorMsg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '异常信息',
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`LogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_QuartzLog
-- ----------------------------
INSERT INTO `Sys_QuartzLog` VALUES ('019bdc40-0639-45f3-8ed7-4f7bb68669ae', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 16:15:00', '2022-09-08 16:15:00', 1, '2022-09-08 16:15:00', NULL, NULL, NULL, '2022-09-08 16:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('03c984b1-d62a-421e-8680-176d8934a840', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 02:15:00', '2022-09-08 02:15:00', 1, '2022-09-08 02:15:00', NULL, NULL, NULL, '2022-09-08 02:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('05b1d45d-cc32-4858-a953-bf8434fb3370', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-07 21:15:00', '2022-09-07 21:15:01', 1, '2022-09-07 21:15:00', NULL, NULL, NULL, '2022-09-07 21:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('066bd885-db2a-4919-9ab2-b02e376361e3', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 20:30:00', '2022-09-07 20:30:00', 1, '2022-09-07 20:30:00', NULL, NULL, NULL, '2022-09-07 20:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('085dae36-9b0a-4ab4-85a8-9eb92fb19706', '22f72d79-7f6f-4c0f-bf4f-c09c529ddc40', '每日统计', 0, '2022-09-08 09:58:33', '2022-09-08 09:58:33', 1, '<!DOCTYPE html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\"><meta content=\"always\" name=\"referrer\"><meta name=\"description\" content=\"全球领先的中文搜索引擎、致力于让网民更便捷地获取信息，找到所求。百度超过千亿的中文网页数据库，可以瞬间找到相关的搜索结果。\"><link rel=\"shortcut icon\" href=\"//www.baidu.com/favicon.ico\" type=\"image/x-icon\"><link rel=\"search\" type=\"application/opensearchdescription+xml\" href=\"//www.baidu.com/content-search.xml\" title=\"百度搜索\"><title>百度一下，你就知道</title><style type=\"text/css\">body{margin:0;padding:0;text-align:center;background:#fff;height:100%}html{overflow-y:auto;color:#000;overflow:-moz-scrollbars;height:100%}body,input{font-size:12px;font-family:\"PingFang SC\",Arial,\"Microsoft YaHei\",sans-serif}a{text-decoration:none}a:hover{text-decoration:underline}img{border:0;-ms-interpolation-mode:bicubic}input{font-size:100%;border:0}body,form{position:relative;z-index:0}#wrapper{height:100%}#head_wrapper.s-ps-islite{padding-bottom:370px}#head_wrapper.s-ps-islite .s_form{position:relative;z-index:1}#head_wrapper.s-ps-islite .fm{position:absolute;bottom:0}#head_wrapper.s-ps-islite .s-p-top{position:absolute;bottom:40px;width:100%;height:181px}#head_wrapper.s-ps-islite #s_lg_img{position:static;margin:33px auto 0 auto;left:50%}#form{z-index:1}.s_form_wrapper{height:100%}#lh{margin:16px 0 5px;word-spacing:3px}.c-font-normal{font:13px/23px Arial,sans-serif}.c-color-t{color:#222}.c-btn,.c-btn:visited{color:#333!important}.c-btn{display:inline-block;overflow:hidden;font-family:inherit;font-weight:400;text-align:center;vertical-align:middle;outline:0;border:0;height:30px;width:80px;line-height:30px;font-size:13px;border-radius:6px;padding:0;background-color:#f5f5f6;cursor:pointer}.c-btn:hover{background-color:#315efb;color:#fff!important}a.c-btn{text-decoration:none}.c-btn-mini{height:24px;width:48px;line-height:24px}.c-btn-primary,.c-btn-primary:visited{color:#fff!important}.c-btn-primary{background-color:#4e6ef2}.c-btn-primary:hover{background-color:#315efb}a:active{color:#f60}#wrapper{position:relative;min-height:100%}#head{padding-bottom:100px;text-align:center}#wrapper{min-width:1250px;height:100%;min-height:600px}#head{position:relative;padding-bottom:0;height:100%;min-height:600px}.s_form_wrapper{height:100%}.quickdelete-wrap{position:relative}.tools{position:absolute;right:-75px}.s-isindex-wrap{position:relative}#head_wrapper.head_wrapper{width:auto}#head_wrapper{position:relative;height:40%;min-height:314px;max-height:510px;width:1000px;margin:0 auto}#head_wrapper .s-p-top{height:60%;min-height:185px;max-height:310px;position:relative;z-index:0;text-align:center}#head_wrapper input{outline:0;-webkit-appearance:none}#head_wrapper .s_btn_wr,#head_wrapper .s_ipt_wr{display:inline-block;zoom:1;background:0 0;vertical-align:top}#head_wrapper .s_ipt_wr{position:relative;width:546px}#head_wrapper .s_btn_wr{width:108px;height:44px;position:relative;z-index:2}#head_wrapper .s_ipt_wr:hover #kw{border-color:#a7aab5}#head_wrapper #kw{width:512px;height:16px;padding:12px 16px;font-size:16px;margin:0;vertical-align:top;outline:0;box-shadow:none;border-radius:10px 0 0 10px;border:2px solid #c4c7ce;background:#fff;color:#222;overflow:hidden;box-sizing:content-box}#head_wrapper #kw:focus{border-color:#4e6ef2!important;opacity:1}#head_wrapper .s_form{width:654px;height:100%;margin:0 auto;text-align:left;z-index:100}#head_wrapper .s_btn{cursor:pointer;width:108px;height:44px;line-height:45px;padding:0;background:0 0;background-color:#4e6ef2;border-radius:0 10px 10px 0;font-size:17px;color:#fff;box-shadow:none;font-weight:400;border:none;outline:0}#head_wrapper .s_btn:hover{background-color:#4662d9}#head_wrapper .s_btn:active{background-color:#4662d9}#head_wrapper .quickdelete-wrap{position:relative}#s_top_wrap{position:absolute;z-index:99;min-width:1000px;width:100%}.s-top-left{position:absolute;left:0;top:0;z-index:100;height:60px;padding-left:24px}.s-top-left .mnav{margin-right:31px;margin-top:19px;display:inline-block;position:relative}.s-top-left .mnav:hover .s-bri,.s-top-left a:hover{color:#315efb;text-decoration:none}.s-top-left .s-top-more-btn{padding-bottom:19px}.s-top-left .s-top-more-btn:hover .s-top-more{display:block}.s-top-right{position:absolute;right:0;top:0;z-index:100;height:60px;padding-right:24px}.s-top-right .s-top-right-text{margin-left:32px;margin-top:19px;display:inline-block;position:relative;vertical-align:top;cursor:pointer}.s-top-right .s-top-right-text:hover{color:#315efb}.s-top-right .s-top-login-btn{display:inline-block;margin-top:18px;margin-left:32px;font-size:13px}.s-top-right a:hover{text-decoration:none}#bottom_layer{width:100%;position:fixed;z-index:302;bottom:0;left:0;height:39px;padding-top:1px;overflow:hidden;zoom:1;margin:0;line-height:39px;background:#fff}#bottom_layer .lh{display:inline;margin-right:20px}#bottom_layer .lh:last-child{margin-left:-2px;margin-right:0}#bottom_layer .lh.activity{font-weight:700;text-decoration:underline}#bottom_layer a{font-size:12px;text-decoration:none}#bottom_layer .text-color{color:#bbb}#bottom_layer a:hover{color:#222}#bottom_layer .s-bottom-layer-content{text-align:center}</style></head><body><div id=\"wrapper\" class=\"wrapper_new\"><div id=\"head\"><div id=\"s-top-left\" class=\"s-top-left s-isindex-wrap\"><a href=\"//news.baidu.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">新闻</a><a href=\"//www.hao123.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">hao123</a><a href=\"//map.baidu.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">地图</a><a href=\"//live.baidu.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">直播</a><a href=\"//haokan.baidu.com/?sfrom=baidu-top\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">视频</a><a href=\"//tieba.baidu.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">贴吧</a><a href=\"//xueshu.baidu.com/\" target=\"_blank\" class=\"mnav c-font-normal c-color-t\">学术</a><div class=\"mnav s-top-more-btn\"><a href=\"//www.baidu.com/more/\" name=\"tj_briicon\" class=\"s-bri c-font-normal c-color-t\" target=\"_blank\">更多</a></div></div><div id=\"u1\" class=\"s-top-right s-isindex-wrap\"><a class=\"s-top-login-btn c-btn c-btn-primary c-btn-mini lb\" style=\"position:relative;overflow:visible\" name=\"tj_login\" href=\"//www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1\">登录</a></div><div id=\"head_wrapper\" class=\"head_wrapper s-isindex-wrap s-ps-islite\"><div class=\"s_form\"><div class=\"s_form_wrapper\"><div id=\"lg\" class=\"s-p-top\"><img hidefocus=\"true\" id=\"s_lg_img\" class=\"index-logo-src\" src=\"//www.baidu.com/img/flexible/logo/pc/index.png\" width=\"270\" height=\"129\" usemap=\"#mp\"><map name=\"mp\"><area style=\"outline:0\" hidefocus=\"true\" shape=\"rect\" coords=\"0,0,270,129\" href=\"//www.baidu.com/s?wd=%E7%99%BE%E5%BA%A6%E7%83%AD%E6%90%9C&amp;sa=ire_dl_gh_logo_texing&amp;rsv_dl=igh_logo_pcs\" target=\"_blank\" title=\"点击一下，了解更多\"></map></div><a href=\"//www.baidu.com/\" id=\"result_logo\"></a><form id=\"form\" name=\"f\" action=\"//www.baidu.com/s\" class=\"fm\"><input type=\"hidden\" name=\"ie\" value=\"utf-8\"> <input type=\"hidden\" name=\"f\" value=\"8\"> <input type=\"hidden\" name=\"rsv_bp\" value=\"1\"> <input type=\"hidden\" name=\"rsv_idx\" value=\"1\"> <input type=\"hidden\" name=\"ch\" value=\"\"> <input type=\"hidden\" name=\"tn\" value=\"baidu\"> <input type=\"hidden\" name=\"bar\" value=\"\"> <span class=\"s_ipt_wr quickdelete-wrap\"><input id=\"kw\" name=\"wd\" class=\"s_ipt\" value=\"\" maxlength=\"255\" autocomplete=\"off\"> </span><span class=\"s_btn_wr\"><input type=\"submit\" id=\"su\" value=\"百度一下\" class=\"bg s_btn\"> </span><input type=\"hidden\" name=\"rn\" value=\"\"> <input type=\"hidden\" name=\"fenlei\" value=\"256\"> <input type=\"hidden\" name=\"oq\" value=\"\"> <input type=\"hidden\" name=\"rsv_pq\" value=\"b9ff093e0000e419\"> <input type=\"hidden\" name=\"rsv_t\" value=\"3635FYbdbC8tlWmudZmYaUnaucNe+RzTzNEGqg/JuniQU10WL5mtMQehIrU\"> <input type=\"hidden\" name=\"rqlang\" value=\"cn\"> <input type=\"hidden\" name=\"rsv_enter\" value=\"1\"> <input type=\"hidden\" name=\"rsv_dl\" value=\"ib\"></form></div></div></div><div id=\"bottom_layer\" class=\"s-bottom-layer s-isindex-wrap\"><div class=\"s-bottom-layer-content\"><p class=\"lh\"><a class=\"text-color\" href=\"//home.baidu.com/\" target=\"_blank\">关于百度</a></p><p class=\"lh\"><a class=\"text-color\" href=\"//ir.baidu.com/\" target=\"_blank\">About Baidu</a></p><p class=\"lh\"><a class=\"text-color\" href=\"//www.baidu.com/duty\" target=\"_blank\">使用百度前必读</a></p><p class=\"lh\"><a class=\"text-color\" href=\"//help.baidu.com/\" target=\"_blank\">帮助中心</a></p><p class=\"lh\"><a class=\"text-color\" href=\"//www.beian.gov.cn/portal/registerSystemInfo?recordcode=11000002000001\" target=\"_blank\">京公网安备11000002000001号</a></p><p class=\"lh\"><a class=\"text-color\" href=\"//beian.miit.gov.cn/\" target=\"_blank\">京ICP证030173号</a></p><p class=\"lh\"><span id=\"year\" class=\"text-color\"></span></p><p class=\"lh\"><span class=\"text-color\">互联网药品信息服务资格证书 (京)-经营性-2017-0020</span></p><p class=\"lh\"><a class=\"text-color\" href=\"//www.baidu.com/licence/\" target=\"_blank\">信息网络传播视听节目许可证 0110516</a></p></div></div></div></div><script type=\"text/javascript\">var date=new Date,year=date.getFullYear();document.getElementById(\"year\").innerText=\"©\"+year+\" Baidu \"</script></body></html>', NULL, NULL, NULL, '2022-09-08 09:58:33', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('0af25760-0338-446b-92bc-14c32e05cf48', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 14:30:00', '2022-09-08 14:30:00', 1, '2022-09-08 14:30:00', NULL, NULL, NULL, '2022-09-08 14:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('0b886c25-96b2-482b-992c-6da0da978ba4', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 03:30:00', '2022-09-08 03:30:00', 1, '2022-09-08 03:30:00', NULL, NULL, NULL, '2022-09-08 03:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('111fe09b-41be-4232-99a1-2b4c86e67aaa', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 02:30:00', '2022-09-08 02:30:00', 1, '2022-09-08 02:30:00', NULL, NULL, NULL, '2022-09-08 02:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('12f5f896-b786-4f5b-bbb9-0492212757d5', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 14:00:00', '2022-09-08 14:00:00', 1, '2022-09-08 14:00:00', NULL, NULL, NULL, '2022-09-08 14:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1447a0d3-9c4f-43e3-8b69-8ee5e7ee3b79', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 08:45:00', '2022-09-08 08:45:00', 1, '2022-09-08 08:45:00', NULL, NULL, NULL, '2022-09-08 08:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('152cd4b4-cbd8-41be-aacb-61427e99c505', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 17:30:00', '2022-09-08 17:30:00', 1, '2022-09-08 17:30:00', NULL, NULL, NULL, '2022-09-08 17:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('16a79c27-3612-454e-9262-e7dbddfe600a', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 12:15:00', '2022-09-08 12:15:00', 1, '2022-09-08 12:15:00', NULL, NULL, NULL, '2022-09-08 12:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('17c0a465-cbef-49f3-a36f-e93f08efc662', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 10:45:00', '2022-09-08 10:45:00', 1, '2022-09-08 10:45:00', NULL, NULL, NULL, '2022-09-08 10:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1811b372-1b2d-4231-b3bc-2bba2ab689e5', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 18:15:00', '2022-09-08 18:15:00', 1, '2022-09-08 18:15:00', NULL, NULL, NULL, '2022-09-08 18:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('182ac040-82c4-455e-9de7-b97f60c89018', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 09:58:32', '2022-09-08 09:58:32', 1, '2022-09-08 09:58:32', NULL, NULL, NULL, '2022-09-08 09:58:32', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1839d26c-e93d-473b-83e5-6aa49fec703e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 19:00:00', '2022-09-08 19:00:00', 1, '2022-09-08 19:00:00', NULL, NULL, NULL, '2022-09-08 19:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('193a3b69-5de0-4d88-9e2a-ab1670863b29', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-08 00:45:00', '2022-09-08 00:45:01', 1, '2022-09-08 00:45:00', NULL, NULL, NULL, '2022-09-08 00:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('19ad3008-463a-403c-9b9d-b6bd4d9d5bb7', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 18:45:00', '2022-09-07 18:45:00', 1, '2022-09-07 18:45:00', NULL, NULL, NULL, '2022-09-07 18:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1ae8398c-5b59-43a3-8145-85eb12f9f668', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:51:36', '2022-09-07 13:51:36', 1, '2022-09-07 13:51:36', NULL, NULL, NULL, '2022-09-07 13:51:36', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1cfd52a3-0d50-403d-939b-ce069cb87155', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 14:45:00', '2022-09-08 14:45:00', 1, '2022-09-08 14:45:00', NULL, NULL, NULL, '2022-09-08 14:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1d7b0608-ae7d-4089-8f73-f36e26463602', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 04:45:00', '2022-09-08 04:45:00', 1, '2022-09-08 04:45:00', NULL, NULL, NULL, '2022-09-08 04:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('1e436a53-ac9b-4250-96a2-d21cbd204b99', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 15:00:00', '2022-09-08 15:00:00', 1, '2022-09-08 15:00:00', NULL, NULL, NULL, '2022-09-08 15:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('2009a710-8c2a-4d25-80cf-92040a14184a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:52:36', '2022-09-07 13:52:36', 1, '2022-09-07 13:52:35', NULL, NULL, NULL, '2022-09-07 13:52:36', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('22334644-16cf-4210-bb08-295f5de840c0', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 16:30:00', '2022-09-08 16:30:00', 1, '2022-09-08 16:30:00', NULL, NULL, NULL, '2022-09-08 16:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('2324474e-48f1-41e7-822e-5b5bbc6b78ec', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 22:30:00', '2022-09-07 22:30:00', 1, '2022-09-07 22:30:00', NULL, NULL, NULL, '2022-09-07 22:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('253148a6-c840-460c-8ef8-0931b15422d1', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 14:45:00', '2022-09-07 14:45:00', 1, '2022-09-07 14:45:00', NULL, NULL, NULL, '2022-09-07 14:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('297c1fbe-b7a8-41a1-90be-c12dfe565f9e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 03:15:00', '2022-09-08 03:15:00', 1, '2022-09-08 03:15:00', NULL, NULL, NULL, '2022-09-08 03:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('2cc1e2b9-29d5-444e-b6a0-4f665903b2b6', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 19:45:00', '2022-09-07 19:45:00', 1, '2022-09-07 19:45:00', NULL, NULL, NULL, '2022-09-07 19:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3265e905-820e-4e81-afea-cf2fb1dbdd4e', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 17:30:00', '2022-09-07 17:30:00', 1, '2022-09-07 17:30:00', NULL, NULL, NULL, '2022-09-07 17:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3559f917-d3b1-4471-a5d3-eae31062b87f', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 13:47:06', '2022-09-07 13:47:06', 1, '2022-09-07 13:47:05', NULL, NULL, NULL, '2022-09-07 13:47:06', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('355cf673-d600-4367-a2b6-4f8f85e45501', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 01:45:00', '2022-09-08 01:45:00', 1, '2022-09-08 01:45:00', NULL, NULL, NULL, '2022-09-08 01:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('38124247-7a3b-4f3b-936c-3a888a371e04', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 02:45:00', '2022-09-08 02:45:00', 1, '2022-09-08 02:45:00', NULL, NULL, NULL, '2022-09-08 02:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('385e6964-214c-4367-9de0-75226a5f2d3c', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 05:15:00', '2022-09-08 05:15:00', 1, '2022-09-08 05:15:00', NULL, NULL, NULL, '2022-09-08 05:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('39163755-82a3-452b-908a-7e92191b2f6f', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 17:00:00', '2022-09-08 17:00:00', 1, '2022-09-08 17:00:00', NULL, NULL, NULL, '2022-09-08 17:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3927ebde-5c42-415c-8a58-c007bc0e3755', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 18:00:00', '2022-09-07 18:00:00', 1, '2022-09-07 18:00:00', NULL, NULL, NULL, '2022-09-07 18:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('39b7c61f-d257-4ec9-bdde-00243f45b15b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 18:30:00', '2022-09-08 18:30:00', 1, '2022-09-08 18:30:00', NULL, NULL, NULL, '2022-09-08 18:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3d727b99-1abc-42a5-ae95-a3b3f0e0cfa3', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 17:30:00', '2022-09-07 17:30:00', 1, '2022-09-07 17:30:00', NULL, NULL, NULL, '2022-09-07 17:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3dd171b4-59b1-4f45-a7e3-b4946f2f9a2b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 15:15:00', '2022-09-07 15:15:00', 1, '2022-09-07 15:15:00', NULL, NULL, NULL, '2022-09-07 15:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('3e57e825-06ce-43ad-8072-11465ec9e20b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 07:30:00', '2022-09-08 07:30:00', 1, '2022-09-08 07:30:00', NULL, NULL, NULL, '2022-09-08 07:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('40b13534-6742-4400-b76b-353735ea789d', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 01:30:00', '2022-09-08 01:30:00', 1, '2022-09-08 01:30:00', NULL, NULL, NULL, '2022-09-08 01:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('41764336-ed1f-454e-9c51-1fde5a2b7bf2', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 06:15:00', '2022-09-08 06:15:00', 1, '2022-09-08 06:15:00', NULL, NULL, NULL, '2022-09-08 06:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('41c08ed1-4f07-4af9-aa14-ade00f85fb4a', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 15:15:00', '2022-09-08 15:15:00', 1, '2022-09-08 15:15:00', NULL, NULL, NULL, '2022-09-08 15:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('4237810e-20b3-4c44-aa29-03a65f2076e3', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 15:45:00', '2022-09-08 15:45:00', 1, '2022-09-08 15:45:00', NULL, NULL, NULL, '2022-09-08 15:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('435f0e64-8949-48c4-9619-da8bb00f6a35', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 15:30:00', '2022-09-08 15:30:00', 1, '2022-09-08 15:30:00', NULL, NULL, NULL, '2022-09-08 15:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('4747d877-e85e-4f4a-93b3-a4b70b6d71af', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 13:00:00', '2022-09-08 13:00:00', 1, '2022-09-08 13:00:00', NULL, NULL, NULL, '2022-09-08 13:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('47f42653-37c9-4bb1-86aa-4ed40df975a7', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-08 12:30:00', '2022-09-08 12:30:01', 1, '2022-09-08 12:30:00', NULL, NULL, NULL, '2022-09-08 12:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('49154394-6694-4b8e-b11c-c933764ad3f4', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 15:30:00', '2022-09-07 15:30:00', 1, '2022-09-07 15:30:00', NULL, NULL, NULL, '2022-09-07 15:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('4bf01701-286e-428f-964c-7414443bce33', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 15:45:00', '2022-09-07 15:45:00', 1, '2022-09-07 15:45:00', NULL, NULL, NULL, '2022-09-07 15:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('4d9fd686-394d-4aa2-83aa-e9eee3c8f126', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 04:00:00', '2022-09-08 04:00:00', 1, '2022-09-08 04:00:00', NULL, NULL, NULL, '2022-09-08 04:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('4fcade99-d8c4-45a2-8d1b-e45b0d75ad18', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 18:00:00', '2022-09-08 18:00:00', 1, '2022-09-08 18:00:00', NULL, NULL, NULL, '2022-09-08 18:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('50119ea9-a3f2-48f0-a6a3-40fb5c7c5878', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 19:00:00', '2022-09-08 19:00:00', 1, '2022-09-08 19:00:00', NULL, NULL, NULL, '2022-09-08 19:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5105db4d-ac25-455a-a464-b787f1bcad73', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 14:30:00', '2022-09-07 14:30:01', 1, '2022-09-07 14:30:00', NULL, NULL, NULL, '2022-09-07 14:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('539a7380-a8e2-4dc7-adfc-f7565b2bbe86', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 16:15:00', '2022-09-07 16:15:00', 1, '2022-09-07 16:15:00', NULL, NULL, NULL, '2022-09-07 16:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5469065a-408e-42e8-b7cd-9f0c6f23d24a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 16:00:00', '2022-09-08 16:00:00', 1, '2022-09-08 16:00:00', NULL, NULL, NULL, '2022-09-08 16:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('54a85d16-9381-4d2b-88b2-8aa2c66c784f', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 13:47:11', '2022-09-07 13:47:11', 1, '2022-09-07 13:47:11', NULL, NULL, NULL, '2022-09-07 13:47:11', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('55656a86-bb1f-458b-8831-55ec7b1435be', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 18:30:00', '2022-09-07 18:30:00', 1, '2022-09-07 18:30:00', NULL, NULL, NULL, '2022-09-07 18:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('562252e1-13ce-4a0d-8b20-b969fc052dba', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 15:30:00', '2022-09-07 15:30:00', 1, '2022-09-07 15:30:00', NULL, NULL, NULL, '2022-09-07 15:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('56752e66-9bca-4a0e-9294-97610b1a6bd2', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 13:30:00', '2022-09-08 13:30:00', 1, '2022-09-08 13:30:00', NULL, NULL, NULL, '2022-09-08 13:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('56e880bf-7f0e-4886-a035-b7f01e4c6925', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 18:15:00', '2022-09-07 18:15:00', 1, '2022-09-07 18:15:00', NULL, NULL, NULL, '2022-09-07 18:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('578d96b1-d56c-4f0b-855f-d5733c87dc48', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-08 11:30:00', '2022-09-08 11:30:01', 1, '2022-09-08 11:30:00', NULL, NULL, NULL, '2022-09-08 11:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('597881a4-54ab-4b81-8742-4a8e5bd74629', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 08:00:00', '2022-09-08 08:00:00', 1, '2022-09-08 08:00:00', NULL, NULL, NULL, '2022-09-08 08:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5b6fe8f3-8338-4fad-bbba-b46fcc3a50da', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 14:00:00', '2022-09-07 14:00:00', 1, '2022-09-07 14:00:00', NULL, NULL, NULL, '2022-09-07 14:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5c4b0ce6-b661-4b39-af93-5139f2922c60', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 1, '2022-09-07 15:00:00', '2022-09-07 15:00:01', 1, '2022-09-07 15:00:00', NULL, NULL, NULL, '2022-09-07 15:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5d3c355e-5692-4878-94a2-b063fcd9be70', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 05:30:00', '2022-09-08 05:30:00', 1, '2022-09-08 05:30:00', NULL, NULL, NULL, '2022-09-08 05:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('5e4f18ec-563b-4fe6-9d45-2c008155713e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 16:00:00', '2022-09-08 16:00:00', 1, '2022-09-08 16:00:00', NULL, NULL, NULL, '2022-09-08 16:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6007bdfa-9ec3-4b99-9590-66a278e773aa', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 21:45:00', '2022-09-07 21:45:00', 1, '2022-09-07 21:45:00', NULL, NULL, NULL, '2022-09-07 21:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('61f1b819-cb18-470d-8d74-e40748d0eff9', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 17:15:00', '2022-09-08 17:15:00', 1, '2022-09-08 17:15:00', NULL, NULL, NULL, '2022-09-08 17:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('63d0b4c2-8fe0-4a85-937a-c0f8ce9e12fb', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 19:30:00', '2022-09-07 19:30:00', 1, '2022-09-07 19:30:00', NULL, NULL, NULL, '2022-09-07 19:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('64a85896-474e-4b5b-bc2b-5cc4d59ee22b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 14:30:00', '2022-09-08 14:30:00', 1, '2022-09-08 14:30:00', NULL, NULL, NULL, '2022-09-08 14:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('67c6992d-71a6-49c0-9473-dc3e209a671b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 08:15:00', '2022-09-08 08:15:00', 1, '2022-09-08 08:15:00', NULL, NULL, NULL, '2022-09-08 08:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('694613e2-48db-429d-a617-2d1962c4a0af', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 09:45:00', '2022-09-08 09:45:00', 1, '2022-09-08 09:45:00', NULL, NULL, NULL, '2022-09-08 09:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('69476c25-7485-4bbc-8cd2-95327757d8ba', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 20:00:00', '2022-09-07 20:00:00', 1, '2022-09-07 20:00:00', NULL, NULL, NULL, '2022-09-07 20:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('695b29a2-677d-425a-81d9-4df7a2c25b83', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 13:30:00', '2022-09-08 13:30:00', 1, '2022-09-08 13:30:00', NULL, NULL, NULL, '2022-09-08 13:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('697714b1-0800-4f09-b3e4-6ddcae5cb281', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-07 22:45:00', '2022-09-07 22:45:01', 1, '2022-09-07 22:45:00', NULL, NULL, NULL, '2022-09-07 22:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6b02da5e-e17e-4a4f-8dac-52ed86a79cea', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 11:00:00', '2022-09-08 11:00:00', 1, '2022-09-08 11:00:00', NULL, NULL, NULL, '2022-09-08 11:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6bc3d6e6-8c5c-4dd8-9ffc-c047b99d7f7f', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 18:30:00', '2022-09-08 18:30:00', 1, '2022-09-08 18:30:00', NULL, NULL, NULL, '2022-09-08 18:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6c4cd1af-d5c7-4953-a804-2989fcc87a59', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 14:15:00', '2022-09-08 14:15:00', 1, '2022-09-08 14:15:00', NULL, NULL, NULL, '2022-09-08 14:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6c6ae225-4c23-4717-92c7-8f176c0df9f1', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:49:23', '2022-09-07 13:49:23', 1, '2022-09-07 13:49:22', NULL, NULL, NULL, '2022-09-07 13:49:23', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6e0b4859-8a58-4103-b78c-12fd5b34483b', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 1, '2022-09-08 10:30:00', '2022-09-08 10:30:01', 1, '2022-09-08 10:30:01', NULL, NULL, NULL, '2022-09-08 10:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6f5a2e7c-e807-4ea4-b1d3-b1a38aabd267', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 01:00:00', '2022-09-08 01:00:00', 1, '2022-09-08 01:00:00', NULL, NULL, NULL, '2022-09-08 01:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6f788d5e-bd34-4567-b39c-56755198eea0', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 06:00:00', '2022-09-08 06:00:00', 1, '2022-09-08 06:00:00', NULL, NULL, NULL, '2022-09-08 06:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('6f943ffc-e2e5-4fde-bb72-11d72f50b372', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 16:00:00', '2022-09-07 16:00:00', 1, '2022-09-07 16:00:00', NULL, NULL, NULL, '2022-09-07 16:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('70055bfa-2c4c-4647-beda-4dd02e26fee0', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 18:00:00', '2022-09-07 18:00:00', 1, '2022-09-07 18:00:00', NULL, NULL, NULL, '2022-09-07 18:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('72824923-6bda-4dc2-a114-919405d16a36', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 08:30:00', '2022-09-08 08:30:00', 1, '2022-09-08 08:30:00', NULL, NULL, NULL, '2022-09-08 08:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('759a5ea8-7c99-4f18-84ef-2238e6b9db21', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 15:30:00', '2022-09-08 15:30:00', 1, '2022-09-08 15:30:00', NULL, NULL, NULL, '2022-09-08 15:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('78e5be47-4c61-4e6a-b524-b819dd279965', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:52:44', '2022-09-07 13:52:44', 1, '2022-09-07 13:52:43', NULL, NULL, NULL, '2022-09-07 13:52:44', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('7e59ca6f-033e-4b27-850e-6a26d2102437', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:48:23', '2022-09-07 13:48:23', 1, '<!DOCTYPE html>\r\n<!--STATUS OK-->\r\n<html>\r\n<head>\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\r\n    <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">\r\n    <meta content=\"always\" name=\"referrer\">\r\n    <script src=\"https://ss1.bdstatic.com/5eN1bjq8AAUYm2zgoY3K/r/www/nocache/imgdata/seErrorRec.js\"></script>\r\n    <title>页面不存在_百度搜索</title>\r\n    <style data-for=\"result\">\r\n        body {color: #333; background: #fff; padding: 0; margin: 0; position: relative; min-width: 700px; font-family: Arial, \'Microsoft YaHei\'; font-size: 12px }\r\n        p, form, ol, ul, li, dl, dt, dd, h3 {margin: 0; padding: 0; list-style: none }\r\n        input {padding-top: 0; padding-bottom: 0; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box } img {border: none; }\r\n        .logo {width: 117px; height: 38px; cursor: pointer }\r\n         #wrapper {_zoom: 1 }\r\n        #head {padding-left: 35px; margin-bottom: 20px; width: 900px }\r\n        .fm {clear: both; position: relative; z-index: 297 }\r\n        .btn, #more {font-size: 14px } \r\n        .s_btn {width: 95px; height: 32px; padding-top: 2px\\9; font-size: 14px; padding: 0; background-color: #ddd; background-position: 0 -48px; border: 0; cursor: pointer }\r\n        .s_btn_h {background-position: -240px -48px }\r\n        .s_btn_wr {width: 97px; height: 34px; display: inline-block; background-position: -120px -48px; *position: relative; z-index: 0; vertical-align: top }\r\n        #foot {}\r\n        #foot span {color: #666 }\r\n        .s_ipt_wr {height: 32px }\r\n        .s_form:after, .s_tab:after {content: \".\"; display: block; height: 0; clear: both; visibility: hidden }\r\n        .s_form {zoom: 1; height: 55px; padding: 0 0 0 10px }\r\n        #result_logo {float: left; margin: 7px 0 0 }\r\n        #result_logo img {width: 101px }\r\n        #head {padding: 0; margin: 0; width: 100%; position: absolute; z-index: 301; min-width: 1000px; background: #fff; border-bottom: 1px solid #ebebeb; position: fixed; _position: absolute; -webkit-transform: translateZ(0) }\r\n        #head .head_wrapper {_width: 1000px }\r\n        #head.s_down {box-shadow: 0 0 5px #888 }\r\n        .fm {clear: none; float: left; margin: 11px 0 0 10px }\r\n        #s_tab {background: #f8f8f8; line-height: 36px; height: 38px; padding: 55px 0 0 121px; float: none; zoom: 1 }\r\n        #s_tab a, #s_tab b {display: inline-block; text-decoration: none; text-align: center; color: #666; font-size: 14px }\r\n        #s_tab b {border-bottom: 2px solid #4E6EF2; color: #222 }\r\n        #s_tab a:hover {color: #323232 }\r\n        #content_left {width: 540px; padding-left: 149px;padding-top: 2px;}\r\n        .to_tieba, .to_zhidao_bottom {margin: 10px 0 0 121px }\r\n        #help {background: #f5f6f5; zoom: 1; padding: 0 0 0 50px; float: right }\r\n        #help a {color: #9195a3; padding-right: 21px; text-decoration: none }\r\n        #help a:hover {color: #333 }\r\n        #foot {position: fixed; bottom:0; width: 100%; background: #f5f6f5; border-top: 1px solid #ebebeb; text-align: left; height: 42px; line-height: 42px; margin-top: 40px; *margin-top: 0; _position:absolute; _bottom:auto; _top:expression(eval(document.documentElement.scrollTop+document.documentElement.clientHeight-this.offsetHeight-(parseInt(this.currentStyle.marginTop,10)||0)-(parseInt(this.currentStyle.marginBottom,10)||0))); }\r\n\r\n        .content_none {padding: 45px 0 25px 121px } .s_ipt_wr.bg,\r\n        .s_btn_wr.bg, #su.bg {background-image: none }\r\n        .s_ipt_wr.bg {background: 0 }\r\n        .s_btn_wr {width: auto; height: auto; border-bottom: 1px solid transparent; *border-bottom: 0 }\r\n        .s_btn {width: 100px; height: 34px; color: white; letter-spacing: 1px; background: #3385ff; border-bottom: 1px solid #2d78f4; outline: medium; *border-bottom: 0; -webkit-appearance: none; -webkit-border-radius: 0 }\r\n        .s_btn:hover {background: #317ef3; border-bottom: 1px solid #2868c8; *border-bottom: 0; box-shadow: 1px 1px 1px #ccc }\r\n        .s_btn:active {background: #3075dc; box-shadow: inset 1px 1px 3px #2964bb; -webkit-box-shadow: inset 1px 1px 3px #2964bb; -moz-box-shadow: inset 1px 1px 3px #2964bb; -o-box-shadow: inset 1px 1px 3px #2964bb }\r\n        #lg {display: none }\r\n        #head .headBlock {margin: -5px 0 6px 121px }\r\n        #content_left .leftBlock {margin-bottom: 14px; padding-bottom: 5px; border-bottom: 1px solid #f3f3f3 }\r\n        .s_ipt_wr {border: 1px solid #b6b6b6; border-color: #7b7b7b #b6b6b6 #b6b6b6 #7b7b7b; background: #fff; display: inline-block; vertical-align: top; width: 592px; margin-right: 0; border-right-width: 0; border-color: #b8b8b8 transparent #ccc #b8b8b8; overflow: hidden }\r\n        .s_ipt_wr.ip_short {width: 439px; }\r\n        .s_ipt_wr:hover, .s_ipt_wr.ipthover {border-color: #999 transparent #b3b3b3 #999 }\r\n        .s_ipt_wr.iptfocus {border-color: #4e6ef2 transparent #4e6ef2 #4e6ef2 }\r\n        .s_ipt_tip {color: #aaa; position: absolute; z-index: -10; font: 16px/22px arial; height: 32px; line-height: 32px; padding-left: 7px; overflow: hidden; width: 526px }\r\n        .s_ipt {width: 526px; height: 22px; font: 16px/18px arial; line-height: 22px\\9; margin: 6px 0 0 7px; padding: 0; background: transparent; border: 0; outline: 0; -webkit-appearance: none }\r\n        #kw {position: relative;display: inline-block;}\r\n        input::-ms-clear {display: none }\r\n        /*Error page css*/\r\n        .norsSuggest {display: inline-block; color: #333; font-family: arial; font-size: 13px; position: relative; } \r\n        .norsTitle {font-size: 22px;font-weight: normal; color: #333; margin: 29px 0 25px 0; }\r\n        .norsTitle2 {font-family: arial; font-size: 13px; color: #666; }\r\n        .norsSuggest ol {margin-left: 47px; }\r\n        .norsSuggest li {margin: 13px 0; }\r\n        #content_right {\r\n    border-left: 1px solid #e1e1e1;\r\n    width: 384px;\r\n    margin-top: 20px;\r\n    float: right;\r\n    padding-left: 17px;\r\n}\r\n#wrapper_wrapper {\r\n    width: 1235px;\r\n    margin-top: 50px;\r\n}\r\n.cr-content {\r\n    width: 351px;\r\n    font-size: 13px;\r\n    line-height: 1.54em;\r\n    color: #333;\r\n    margin-top: 8px;\r\n    margin-left: 19px;\r\n    word-wrap: break-word;\r\n    word-break: normal;\r\n}\r\n@media screen and (max-width: 1217px) {\r\n    #wrapper_wrapper {\r\n        width: 1002px;\r\n    }\r\n    #wrapper_wrapper #content_right {\r\n        width: 271px;\r\n    }\r\n    #wrapper_wrapper .cr-content {\r\n        width: 259px;\r\n    }\r\n}\r\n.opr-toplist-title {\r\n    position: relative;\r\n    font-size: 14px;\r\n    line-height: 1.29em;\r\n    font-weight: 700;\r\n    margin-bottom: 3px;\r\n    font: 14px/22px Arial,sans-serif;\r\n    color: #222;\r\n    font-weight: 400;\r\n}\r\n.opr-toplist-table {\r\n    width: 100%;\r\n    border-collapse: collapse;\r\n    border-spacing: 0;\r\n    font-size: 13px;\r\n}\r\n.opr-toplist-table th,td {\r\n    line-height: 1.54;\r\n    border-bottom: 1px solid #f3f3f3;\r\n    text-align: left;\r\n}\r\n.opr-toplist-table thead th {\r\n    padding-top: 4px;\r\n    padding-bottom: 4px;\r\n    font-weight: 400;\r\n    color: #666;\r\n    white-space: nowrap;\r\n    background-color: #fafafa;  \r\n}\r\n.opr-toplist-table .opr-toplist-right {\r\n    text-align: right;\r\n    white-space: nowrap;\r\n}\r\n.opr-toplist-table td {\r\n    width: 100%;\r\n    font-size: 13px;\r\n    padding-top: 5.5px;\r\n    padding-bottom: 5.5px;\r\n    vertical-align: top;\r\n}\r\n.opr-toplist-table a:hover {\r\n    color: #315EFB;\r\n    text-decoration: underline;\r\n}\r\n.opr-toplist-table td{\r\n    padding-top: 5px 0;\r\n    border: none!important;\r\n    height: 20px;\r\n    line-height: 20px!important;\r\n}\r\n.opr-item-text{\r\n    font: 14px/22px Arial,sans-serif;\r\n    color: #2440B3;\r\n}\r\n.opr-index-item {\r\n    display: inline-block;\r\n    padding:1px 0;\r\n    color: #9195A3;\r\n    width: 20px;\r\n    height: 16px;\r\n    line-height: 100%;\r\n    font-size: 15px;\r\n    margin-right: 1px;\r\n    text-align: left;\r\n}\r\n.opr-index-hot1 {\r\n    color: #f54545;\r\n}\r\n\r\n.opr-index-hot2 {\r\n    color: #ff8547;\r\n}\r\n.opr-index-hot3 {\r\n    color: #ffac38;\r\n}\r\n.opr-item-text {\r\n    text-decoration: none;  \r\n}\r\n.opr-toplist-info {\r\n    color: #666;\r\n    text-align: right;\r\n    margin-top: 5px;\r\n}\r\n.opr-toplist-info>a {\r\n    color: #666;\r\n}\r\n/* 新加 */\r\n@font-face {\r\n  font-family: \'cIconfont\';\r\n  src: url(\'./font/iconfont.eot\');\r\n  src: url(\'./font/iconfont.eot?#iefix\') format(\'embedded-opentype\'),\r\n    url(\'./font/iconfont.woff2\') format(\'woff2\'),\r\n    url(\'./font/iconfont.woff\') format(\'woff\'),\r\n    url(\'./font/iconfont.ttf\') format(\'truetype\'),\r\n    url(\'./font/iconfont.svg#iconfont\') format(\'svg\');\r\n}\r\n.s_form {\r\n    zoom: 1;\r\n    padding: 15px 0 4px 16px;\r\n    height: 40px;\r\n    font-size: 0;\r\n}\r\n#result_logo{\r\n    margin-top: 2px;\r\n}\r\n#result_logo img {\r\n    width: 101px;\r\n    height: 33px;\r\n}\r\n.fm {\r\n    margin-left: 19px;\r\n}\r\n.s_ipt_wr{\r\n    box-sizing: border-box;\r\n    height:40px;\r\n    line-height: 40px;\r\n    border: 2px solid #c4c7ce;\r\n    border-radius: 10px 0 0 10px;\r\n    border-right: 0;\r\n    overflow: visible;\r\n}\r\n.s_btn_wr .s_btn{\r\n    cursor: pointer;\r\n    width: 112px;\r\n    height: 40px;\r\n    line-height: 41px;\r\n    line-height: 40px\\9;\r\n    background-color: #4e6ef2;\r\n    border-radius: 0 10px 10px 0;\r\n    font-size: 17px;\r\n    box-shadow: none;\r\n    font-weight: 400;\r\n    border: 0;\r\n    outline: 0;\r\n    letter-spacing: normal;\r\n}\r\n.s_btn_wr .s_btn:hover{\r\n    background: #4662d9;\r\n}\r\n.s_ipt_wr.ip_short{\r\n    width: 480px;\r\n}\r\n#kw{\r\n    width: 465px;\r\n    height: 38px;\r\n    font: 16px/18px arial;\r\n    padding: 10px 0 10px 14px;\r\n    margin: 0;\r\n    background: transparent;\r\n    border: 0;\r\n}\r\n.fm{\r\n    margin: 0 0 15px 16px;\r\n    clear: none;\r\n    float: left;\r\n    position: relative;\r\n}\r\n#head{\r\n    border-bottom: none;\r\n    position: relative;\r\n}\r\n#u {\r\n    height: 40px;\r\n    line-height: 40px;\r\n    padding-right: 30px;\r\n    z-index: 301;\r\n    display: inline-block;\r\n    float: right;\r\n    color: #999;\r\n}\r\n#u .toindex{\r\n    font-family: Arial, \'Microsoft YaHei\';\r\n    font-size: 13px;\r\n    color: #222222;\r\n    text-align: right;\r\n    text-decoration: none;\r\n}\r\n#u .toindex:hover{\r\n    color: #2640b3;\r\n}\r\n.s-tab-item:hover::before{\r\n    color: #222 !important;\r\n}\r\n#s_tab {\r\n    position: absolute;\r\n    left: 0;\r\n    background: none;\r\n    line-height: 36px;\r\n    height: 38px;\r\n    padding: 2px 0 0 150px;\r\n    float: none;\r\n    zoom: 1;\r\n    color: #626675;\r\n    overflow: hidden;\r\n}\r\n#s_tab a, #s_tab b {\r\n    display: inline-block;\r\n    margin-right: 24px;\r\n    min-width: 44px;\r\n    text-decoration: none;\r\n    font-size: 14px;\r\n    text-align: left;\r\n    line-height: 28px;\r\n    color: #222222;\r\n    font-weight: normal;\r\n}\r\n#s_tab a{\r\n    color: #626675;\r\n}\r\n#s_tab .s-tab-item:before {\r\n    display: inline-block;\r\n    margin-right: 2px;\r\n    width: 14px;\r\n    font-family: \'cIconfont\'!important;\r\n    font-style: normal;\r\n    -webkit-font-smoothing: antialiased;\r\n    background: initial;\r\n    color: #c0c2c8;\r\n}\r\n#s_tab .cur-tab:before {\r\n    font-family: \'cIconfont\'!important;\r\n    color: #626675;\r\n    margin-right: 2px;\r\n    content: \'\\e608\';\r\n}\r\n#s_tab .s-tab-news:before {\r\n    content: \'\\e606\';\r\n}\r\n#s_tab .s-tab-video:before {\r\n    content: \'\\e604\';\r\n}\r\n#s_tab .s-tab-pic:before {\r\n    content: \'\\e607\';\r\n}\r\n#s_tab .s-tab-zhidao:before {\r\n    content: \'\\e633\';\r\n}\r\n#s_tab .s-tab-wenku:before {\r\n    content: \'\\e605\';\r\n}\r\n#s_tab .s-tab-tieba:before {\r\n    content: \'\\e609\';\r\n}\r\n#s_tab .s-tab-map:before {\r\n    content: \'\\e630\';\r\n}\r\n#s_tab .s-tab-b2b:before {\r\n    content: \'\\e603\';\r\n}\r\n#content_right{\r\n    border-left: none;\r\n}\r\n.norsTitle{\r\n    font-family: Arial, \'Microsoft YaHei\';\r\n    font-size: 18px;\r\n    color: #222222;\r\n    text-align: justify;\r\n    line-height: 18px;\r\n}\r\n.norsTitle{\r\n    margin-bottom: 28px;\r\n}\r\n.norsTitle2{\r\n    font-family: Arial, \'Microsoft YaHei\';\r\n    font-size: 14px;\r\n    color: #333333;\r\n    text-align: justify;\r\n    line-height: 14px;\r\n    margin-bottom: 12px;\r\n}\r\n.norsSuggest a{\r\n    color: #2440b3;\r\n    text-decoration: none;\r\n}\r\n.norsSuggest a:hover{\r\n    color: #315efb;\r\n    text-decoration: underline;\r\n}\r\n#foot{\r\n    background: #f5f5f6;\r\n    color: #9195a3;\r\n    border-top: none;\r\n}\r\n#help{\r\n    float:left;\r\n    padding-left:150px\r\n}\r\n    </style>\r\n</head>\r\n\r\n<body link=\"#0000cc\">\r\n    <div id=\"wrapper\" class=\"wrapper_l\">\r\n        <div id=\"head\">\r\n            <div class=\"head_wrapper\">\r\n                <div class=\"s_form\">\r\n                    <div class=\"s_form_wrapper\">\r\n                        <a href=\"//www.baidu.com/\" id=\"result_logo\"><img src=\"//www.baidu.com/img/flexible/logo/pc/result@2.png\" alt=\"到百度首页\" title=\"到百度首页\"></a>\r\n                        <form id=\"form\" name=\"f\" action=\"//www.baidu.com/s\" class=\"fm\">\r\n                            <input type=\"hidden\" name=\"ie\" value=\"utf-8\">\r\n                            <input type=\"hidden\" name=\"f\" value=\"8\">\r\n                            <input type=\"hidden\" name=\"rsv_bp\" value=\"1\">\r\n                            <input type=\"hidden\" name=\"ch\" value=\"\">\r\n                            <input type=\"hidden\" name=\"tn\" value=\"baiduerr\">\r\n                            <input type=\"hidden\" name=\"bar\" value=\"\">\r\n                            <span class=\"bg s_ipt_wr iptfocus\">\r\n                                <input id=\"kw\" name=\"wd\" class=\"s_ipt\" value=\"\" maxlength=\"255\" autocomplete=\"off\" autofocus>\r\n                            </span><span class=\"bg s_btn_wr\">\r\n                                <input type=\"submit\" id=\"su\" value=\"百度一下\" class=\"bg s_btn\">\r\n                            </span>\r\n                        </form>\r\n                    </div>\r\n                    <div id=\"u\">\r\n                        <a class=\"toindex\" href=\"//www.baidu.com/\">百度首页</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    <div class=\"s_tab\" id=\"s_tab\">\r\n        <b class=\"cur-tab\">网页</b>\r\n        <a href=\"//www.baidu.com/?tn=news\" class=\"s-tab-item s-tab-news\" wdfield=\"kw\">资讯</a>\r\n        <a href=\"//v.baidu.com/\" class=\"s-tab-item s-tab-video\">视频</a>\r\n        <a href=\"//image.baidu.com/\" class=\"s-tab-item s-tab-pic\">图片</a>\r\n        <a href=\"//zhidao.baidu.com/\" class=\"s-tab-item s-tab-zhidao\">知道</a>\r\n        <a href=\"//wenku.baidu.com/\" class=\"s-tab-item s-tab-wenku\">文库</a>\r\n        <a href=\"//tieba.baidu.com/index.html\" class=\"s-tab-item s-tab-tieba\">贴吧</a>\r\n        <a href=\"//map.baidu.com/\" class=\"s-tab-item s-tab-map\">地图</a>\r\n        <a href=\"//b2b.baidu.com/s?fr=wwwt\" class=\"s-tab-item s-tab-b2b\">采购</a>\r\n        <a href=\"//www.baidu.com/more/\" class=\"s-tab-item s-tab-more\">更多</a>\r\n    </div>\r\n    <div id=\"wrapper_wrapper\">\r\n        <div id=\"content_left\">\r\n            <div class=\"nors\">\r\n                <div class=\"norsSuggest\">\r\n                    <h3 class=\"norsTitle\">很抱歉，您要访问的页面不存在！</h3>\r\n                    <p class=\"norsTitle2\">温馨提示 :</p>\r\n                    <p class=\"norsTitle2\">请检查您访问的网址是否正确</p>\r\n                    <p class=\"norsTitle2\">如果您不能确认访问的网址，请浏览<a href=\"//www.baidu.com/more/index.html\">百度更多</a>页面查看更多网址。</p>\r\n                    <p class=\"norsTitle2\">回到顶部重新发起搜索</p>\r\n                    <p class=\"norsTitle2\">如有任何意见或建议，请及时<a href=\"http://qingting.baidu.com/index\">反馈给我们</a>。</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div id=\"foot\">\r\n        <span id=\"help\">\r\n            <a href=\"http://help.baidu.com/question\" target=\"_blank\">帮助</a>\r\n            <a href=\"http://www.baidu.com/search/jubao.html\" target=\"_blank\">举报</a>\r\n            <a href=\"http://jianyi.baidu.com\" target=\"_blank\">用户反馈</a>\r\n        </span>\r\n    </div>\r\n</body>\r\n<script>\r\n(function () {\r\n        var LOGURL = \'https://sp1.baidu.com/5b1ZeDe5KgQFm2e88IuM_a/cm.gif\';\r\n        var params = \'type=wwwerror&terminal=www\';\r\n        var img = new Image();\r\n        img.src = LOGURL + \'?\' + params;\r\n    })();\r\n    (function () {\r\n        if(window.recommend && window.recommend.query && window.recommend.query.length > 0) {\r\n            var recommendWrapper = document.createElement(\'div\');\r\n            var recommendHtml = \'<div class=\"cr-content\"><div class=\"opr-toplist-title\">\' + window.recommend.title + \'</div><table class=\"opr-toplist-table\">\';\r\n            var queryArray = window.recommend.query;\r\n            var itemUrl = \'\';\r\n            for(var i = 1; i < (queryArray.length+1); i++) {\r\n                itemUrl = \'//www.baidu.com/s?word=\' + encodeURIComponent(queryArray[i-1].word) + \'&sa=\' + queryArray[i-1].sa + \'&tn=\' + queryArray[i-1].tn;\r\n                if (queryArray[i-1].rsv_dl) {\r\n                    itemUrl += \"&rsv_dl=\" + queryArray[i-1].rsv_dl;\r\n                }\r\n                \r\n                if (i < 4) {\r\n                    recommendHtml += \'<tr><td><span class=\"opr-index-hot\' + i + \' opr-index-item\">\' + i + \'</span><a target=\"_blank\" href=\"\' + itemUrl +\'\" class=\"opr-item-text\">\' + queryArray[i-1].word + \'</a></td></tr>\';\r\n                } else {\r\n                    recommendHtml += \'<tr><td><span class=\"opr-index-item\">\' + i + \'</span><a target=\"_blank\" href=\"\' + itemUrl +\'\" class=\"opr-item-text\">\' + queryArray[i-1].word + \'</a></td></tr>\';\r\n                }\r\n            }\r\n            recommendHtml += \'</tbody></table></div>\';\r\n            recommendWrapper.setAttribute(\'id\', \'content_right\');\r\n            document.getElementById(\'wrapper_wrapper\').insertBefore(recommendWrapper, document.getElementById(\'content_left\'));\r\n            var recommend = document.getElementById(\'content_right\');\r\n            recommend.innerHTML = recommendHtml;\r\n        }\r\n})();\r\n(function(){\r\n    var bds = {\r\n        util: {}\r\n    };\r\n    var c = document.getElementById(\'kw\').parentNode;\r\n\r\n    bds.util.getWinWidth = function(){\r\n        return window.document.documentElement.clientWidth;\r\n    };\r\n\r\n    bds.util.setFormWidth = function(){\r\n        var width = bds.util.getWinWidth();\r\n        if(width < 1217)    {bds.util.setClass(c, \'ip_short\', \'add\')}\r\n        else                {bds.util.setClass(c, \'ip_short\', \'remove\')};\r\n    };\r\n\r\n    bds.util.setClass = function(obj, class_name, set) {\r\n        var ori_class = obj.className,\r\n            has_class_p = -1,\r\n            ori_class_arr = [],\r\n            new_class = \'\';\r\n\r\n        if(ori_class.length) ori_class_arr = ori_class.split(\' \');\r\n\r\n        for( i in ori_class_arr) {\r\n            if(ori_class_arr[i] == class_name) has_class_p = i;\r\n        }\r\n\r\n        if( set == \'remove\' && has_class_p >= 0) {\r\n            ori_class_arr.splice(has_class_p, 1);\r\n            new_class = ori_class_arr.join(\' \');\r\n            obj.className = new_class;\r\n        } else if( set == \'add\' && has_class_p < 0) {\r\n            ori_class_arr.push(class_name);\r\n            new_class = ori_class_arr.join(\' \');\r\n            obj.className = new_class;\r\n        }\r\n    }\r\n    bds.util.setFormWidth();\r\n\r\n    if (typeof document.addEventListener != \"undefined\") {\r\n        window.addEventListener(\'resize\', bds.util.setFormWidth, false);\r\n        document.getElementById(\'kw\').addEventListener(\'focus\', function(){bds.util.setClass(c,\'iptfocus\', \'add\');}, false);\r\n        document.getElementById(\'kw\').addEventListener(\'blur\', function(){bds.util.setClass(c,\'iptfocus\', \'remove\');}, false);\r\n    } else {\r\n        window.attachEvent(\'onresize\', bds.util.setFormWidth, false);\r\n        document.getElementById(\'kw\').attachEvent(\'onfocus\', function(){bds.util.setClass(c,\'iptfocus\', \'add\');}, false);\r\n        document.getElementById(\'kw\').attachEvent(\'onblur\', function(){bds.util.setClass(c,\'iptfocus\', \'remove\');}, false);\r\n    } \r\n\r\n})();\r\n\r\n\r\n</script>\r\n\r\n</html>\r\n', NULL, NULL, NULL, '2022-09-07 13:48:23', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('7eb82b85-ecf3-4180-baa7-020ae66df370', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-07 15:00:00', '2022-09-07 15:00:01', 1, '2022-09-07 15:00:00', NULL, NULL, NULL, '2022-09-07 15:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('7f270194-5054-4323-9a50-ee0ca928eb59', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 16:00:00', '2022-09-07 16:00:00', 1, '2022-09-07 16:00:00', NULL, NULL, NULL, '2022-09-07 16:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('7f7d9d59-c119-4d83-853c-ff13f2ae8f5f', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 21:00:00', '2022-09-07 21:00:00', 1, '2022-09-07 21:00:00', NULL, NULL, NULL, '2022-09-07 21:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('80065734-a65b-4237-87d8-2b9f8cad7db8', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 14:30:00', '2022-09-07 14:30:01', 1, '2022-09-07 14:30:00', NULL, NULL, NULL, '2022-09-07 14:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('81bef6af-05d0-4916-860e-0a34fe9dc860', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 14:00:00', '2022-09-07 14:00:00', 1, '2022-09-07 14:00:00', NULL, NULL, NULL, '2022-09-07 14:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('84ca61a3-03a6-45a9-ac49-4f91f59a6121', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 14:04:16', '2022-09-07 14:04:16', 1, '2022-09-07 14:04:15', NULL, NULL, NULL, '2022-09-07 14:04:16', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('85100279-44dd-497c-8d7d-c3136146291a', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 13:45:00', '2022-09-08 13:45:00', 1, '2022-09-08 13:45:00', NULL, NULL, NULL, '2022-09-08 13:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('85daccdd-80c5-4e32-b377-0b7d61d27fac', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 16:45:00', '2022-09-07 16:45:00', 1, '2022-09-07 16:45:00', NULL, NULL, NULL, '2022-09-07 16:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('88196bca-4d38-4675-8bbf-b77cf87cf117', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 22:15:00', '2022-09-07 22:15:00', 1, '2022-09-07 22:15:00', NULL, NULL, NULL, '2022-09-07 22:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('88bd0d51-e698-4513-b74a-1ead8993dae4', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 11:00:00', '2022-09-08 11:00:00', 1, '2022-09-08 11:00:00', NULL, NULL, NULL, '2022-09-08 11:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('894bdb14-f95a-4e28-82fe-48a0bdc71217', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 17:45:00', '2022-09-08 17:45:00', 1, '2022-09-08 17:45:00', NULL, NULL, NULL, '2022-09-08 17:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('8afcee6c-c8f7-40e0-857c-24443c77c34a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 12:30:00', '2022-09-08 12:30:00', 1, '2022-09-08 12:30:00', NULL, NULL, NULL, '2022-09-08 12:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('8eba85ca-9d21-4140-98fb-f24eb719bbce', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 17:15:00', '2022-09-07 17:15:00', 1, '2022-09-07 17:15:00', NULL, NULL, NULL, '2022-09-07 17:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('8fcba9c3-1a59-468d-91a3-83200d97b3a8', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 14:04:17', '2022-09-07 14:04:17', 1, '2022-09-07 14:04:16', NULL, NULL, NULL, '2022-09-07 14:04:17', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('90013f44-693f-40e0-8bce-a64cfbed7fcb', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 23:00:00', '2022-09-07 23:00:00', 1, '2022-09-07 23:00:00', NULL, NULL, NULL, '2022-09-07 23:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('929e7967-8dd9-451a-82c4-f1a3fd3250d1', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 09:30:00', '2022-09-08 09:30:00', 1, '2022-09-08 09:30:00', NULL, NULL, NULL, '2022-09-08 09:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('93ed973c-df9d-43cb-9731-98749f075c0c', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-07 20:15:00', '2022-09-07 20:15:01', 1, '2022-09-07 20:15:00', NULL, NULL, NULL, '2022-09-07 20:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('940659cc-30db-4a4e-ae05-f1c9987bcf91', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 19:15:00', '2022-09-07 19:15:00', 1, '2022-09-07 19:15:00', NULL, NULL, NULL, '2022-09-07 19:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('9565bb19-6f57-4ef9-96b8-b4700ae19996', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 04:30:00', '2022-09-08 04:30:00', 1, '2022-09-08 04:30:00', NULL, NULL, NULL, '2022-09-08 04:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('95767d79-ba6a-43ff-8a6e-103a95a8cf01', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 11:15:00', '2022-09-08 11:15:00', 1, '2022-09-08 11:15:00', NULL, NULL, NULL, '2022-09-08 11:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('971b8b0d-f2e1-4cbd-8043-ccf511bb6df8', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 19:15:00', '2022-09-08 19:15:00', 1, '2022-09-08 19:15:00', NULL, NULL, NULL, '2022-09-08 19:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('9ee03cbc-cea0-4e80-b9c8-aa978db09b3a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 12:00:00', '2022-09-08 12:00:00', 1, '2022-09-08 12:00:00', NULL, NULL, NULL, '2022-09-08 12:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('9f14fe2c-0f3c-4b1c-9469-c215810eaa72', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 12:00:00', '2022-09-08 12:00:00', 1, '2022-09-08 12:00:00', NULL, NULL, NULL, '2022-09-08 12:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('9fed446d-18a0-4865-9cc2-03319f2bf68e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 11:45:00', '2022-09-08 11:45:00', 1, '2022-09-08 11:45:00', NULL, NULL, NULL, '2022-09-08 11:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('a0820102-fa23-4b3a-8d30-3742048dbc0b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 17:00:00', '2022-09-07 17:00:00', 1, '2022-09-07 17:00:00', NULL, NULL, NULL, '2022-09-07 17:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('a2a9a1b6-aa0d-418c-bfdc-0adf575637f6', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 1, '2022-09-08 10:30:00', '2022-09-08 10:30:01', 1, '2022-09-08 10:30:00', NULL, NULL, NULL, '2022-09-08 10:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('a32999d5-6eb2-4b7b-b8d4-d8399bb303b6', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 20:45:00', '2022-09-07 20:45:00', 1, '2022-09-07 20:45:00', NULL, NULL, NULL, '2022-09-07 20:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('a446dab3-1af9-4239-bb85-c809f97defdf', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 13:15:00', '2022-09-08 13:15:00', 1, '2022-09-08 13:15:00', NULL, NULL, NULL, '2022-09-08 13:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('a90cfbb6-2038-4b0f-81fe-c687615aff9b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 03:00:00', '2022-09-08 03:00:00', 1, '2022-09-08 03:00:00', NULL, NULL, NULL, '2022-09-08 03:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('abff514d-1cdc-4df7-ab9e-5a2cef939b8b', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 14:00:00', '2022-09-08 14:00:00', 1, '2022-09-08 14:00:00', NULL, NULL, NULL, '2022-09-08 14:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ad516c43-c475-4daf-9d62-70a7a2334a0a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:51:59', '2022-09-07 13:51:59', 1, '2022-09-07 13:51:59', NULL, NULL, NULL, '2022-09-07 13:51:59', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ade45f6f-228a-4849-a33f-f1252e9e2ab7', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 16:45:00', '2022-09-08 16:45:00', 1, '2022-09-08 16:45:00', NULL, NULL, NULL, '2022-09-08 16:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('af69557f-552e-4841-bfb4-a6e0e7814482', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 07:45:00', '2022-09-08 07:45:00', 1, '2022-09-08 07:45:00', NULL, NULL, NULL, '2022-09-08 07:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('af844347-b965-4d64-801d-a8c3a2b7acdf', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 16:30:00', '2022-09-08 16:30:00', 1, '2022-09-08 16:30:00', NULL, NULL, NULL, '2022-09-08 16:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('b223b6c7-b9c7-4038-a423-d11e0fe5818e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 05:00:00', '2022-09-08 05:00:00', 1, '2022-09-08 05:00:00', NULL, NULL, NULL, '2022-09-08 05:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('b40977f3-5ca8-4a6e-8672-664ab61de43d', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 15:00:00', '2022-09-08 15:00:00', 1, '2022-09-08 15:00:00', NULL, NULL, NULL, '2022-09-08 15:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('b6921b5d-9627-43e4-b538-5286ec601a51', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 03:45:00', '2022-09-08 03:45:00', 1, '2022-09-08 03:45:00', NULL, NULL, NULL, '2022-09-08 03:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('b8cfaa7a-67b2-4102-a263-32651c1c79c9', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 10:00:00', '2022-09-08 10:00:00', 1, '2022-09-08 10:00:00', NULL, NULL, NULL, '2022-09-08 10:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ba6acdf1-b1d4-4a00-95e2-0d62b5ebf4e6', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 14:15:00', '2022-09-07 14:15:00', 1, '2022-09-07 14:15:00', NULL, NULL, NULL, '2022-09-07 14:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ba826dd0-44bf-416d-890b-4650c9fc2a7c', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 23:15:00', '2022-09-07 23:15:00', 1, '2022-09-07 23:15:00', NULL, NULL, NULL, '2022-09-07 23:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('bd82789b-fb94-4e38-84c3-fe06d31e0322', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 01:15:00', '2022-09-08 01:15:00', 1, '2022-09-08 01:15:00', NULL, NULL, NULL, '2022-09-08 01:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('bea77b18-39d3-4659-af35-d30a586ec366', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 1, '2022-09-08 11:30:00', '2022-09-08 11:30:01', 1, '2022-09-08 11:30:00', NULL, NULL, NULL, '2022-09-08 11:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('c275086e-fbcc-41ba-9ea2-564f0824494e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 00:00:00', '2022-09-08 00:00:00', 1, '2022-09-08 00:00:00', NULL, NULL, NULL, '2022-09-08 00:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('c304b213-0f87-4180-94fc-7d025735ccea', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 17:45:00', '2022-09-07 17:45:00', 1, '2022-09-07 17:45:00', NULL, NULL, NULL, '2022-09-07 17:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('c33ff891-a5c1-4c80-b440-4cb7f0fa18a2', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 13:00:00', '2022-09-08 13:00:00', 1, '2022-09-08 13:00:00', NULL, NULL, NULL, '2022-09-08 13:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('c553687e-8a2b-4df3-a227-78c25c0a227f', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 17:00:00', '2022-09-07 17:00:00', 1, '2022-09-07 17:00:00', NULL, NULL, NULL, '2022-09-07 17:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('c644353b-4431-4a1a-b259-379e19fcbb7a', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 1, '2022-09-07 13:52:34', '2022-09-07 13:52:35', 1, '2022-09-07 13:52:35', NULL, NULL, NULL, '2022-09-07 13:52:34', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('cd75b5ee-5b84-44b3-a5d4-aa17f0720a60', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 18:45:00', '2022-09-08 18:45:00', 1, '2022-09-08 18:45:00', NULL, NULL, NULL, '2022-09-08 18:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ce3b2665-ea79-43b2-85a3-f284f6449099', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 19:00:00', '2022-09-07 19:00:00', 1, '2022-09-07 19:00:00', NULL, NULL, NULL, '2022-09-07 19:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ce61721f-9388-49d4-9252-c47d470d3001', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 12:45:00', '2022-09-08 12:45:00', 1, '2022-09-08 12:45:00', NULL, NULL, NULL, '2022-09-08 12:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('cf05c0ed-c875-42d6-9ee0-a5b5ebc8192c', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 23:45:00', '2022-09-07 23:45:00', 1, '2022-09-07 23:45:00', NULL, NULL, NULL, '2022-09-07 23:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('d1132c8a-4c96-4601-9bf3-7a4bc724570e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 09:00:00', '2022-09-08 09:00:00', 1, '2022-09-08 09:00:00', NULL, NULL, NULL, '2022-09-08 09:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('dd9c4557-84c1-4329-ac6f-7142cab39f72', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 19:00:00', '2022-09-07 19:00:00', 1, '2022-09-07 19:00:00', NULL, NULL, NULL, '2022-09-07 19:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ddc4c31d-a62a-476a-8846-e4ab12dc7a05', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:47:05', '2022-09-07 13:47:05', 1, 'An invalid request URI was provided. The request URI must either be an absolute URI or BaseAddress must be set.', NULL, NULL, NULL, '2022-09-07 13:47:05', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('de01406f-b0ee-4fc6-ab76-0b83b72616b1', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 06:30:00', '2022-09-08 06:30:00', 1, '2022-09-08 06:30:00', NULL, NULL, NULL, '2022-09-08 06:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('deaa692a-f842-48ef-adbf-35da08c3ce53', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 18:30:00', '2022-09-07 18:30:00', 1, '2022-09-07 18:30:00', NULL, NULL, NULL, '2022-09-07 18:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e0046484-75d5-4dfb-aa0b-d940c0d5d51d', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 05:45:00', '2022-09-08 05:45:00', 1, '2022-09-08 05:45:00', NULL, NULL, NULL, '2022-09-08 05:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e389613d-b43b-41ca-9ff1-7eb8135bdc78', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 04:15:00', '2022-09-08 04:15:00', 1, '2022-09-08 04:15:00', NULL, NULL, NULL, '2022-09-08 04:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e41b9c20-81ae-4921-b7b2-d5ea5667d21e', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 17:00:00', '2022-09-08 17:00:00', 1, '2022-09-08 17:00:00', NULL, NULL, NULL, '2022-09-08 17:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e66e348a-9299-499a-b5b6-4c0bbf52602e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 21:30:00', '2022-09-07 21:30:00', 1, '2022-09-07 21:30:00', NULL, NULL, NULL, '2022-09-07 21:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e6714ea7-a79b-479a-9268-92c6bf666390', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 13:55:27', '2022-09-07 13:55:27', 1, '2022-09-07 13:55:27', NULL, NULL, NULL, '2022-09-07 13:55:27', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e6e4c327-bedd-420c-8cd0-e0ebdf85e6ce', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 06:45:00', '2022-09-08 06:45:00', 1, '2022-09-08 06:45:00', NULL, NULL, NULL, '2022-09-08 06:45:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('e7450c02-cd92-425f-9556-0a12e43ee45f', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 17:30:00', '2022-09-08 17:30:00', 1, '2022-09-08 17:30:00', NULL, NULL, NULL, '2022-09-08 17:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ee139faa-b20a-4e2f-b0b8-4039b8aa512c', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 18:00:00', '2022-09-08 18:00:00', 1, '2022-09-08 18:00:00', NULL, NULL, NULL, '2022-09-08 18:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ef1c0d76-76f5-4d27-87a5-4ad3efd4fcfc', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 09:15:00', '2022-09-08 09:15:00', 1, '2022-09-08 09:15:00', NULL, NULL, NULL, '2022-09-08 09:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ef5a4ba4-ec1d-4a41-81e0-9fd9f04028ea', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 22:00:00', '2022-09-07 22:00:00', 1, '2022-09-07 22:00:00', NULL, NULL, NULL, '2022-09-07 22:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f08644a9-75f6-4c98-a9f3-0ce061a41928', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 16:30:00', '2022-09-07 16:30:00', 1, '2022-09-07 16:30:00', NULL, NULL, NULL, '2022-09-07 16:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f08f0aae-28b3-4fda-89ab-2d4e772265e8', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-07 23:30:00', '2022-09-07 23:30:00', 1, '2022-09-07 23:30:00', NULL, NULL, NULL, '2022-09-07 23:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f11c9782-0edc-4a94-9b02-9e38f467618e', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-08 10:00:00', '2022-09-08 10:00:00', 1, '2022-09-08 10:00:00', NULL, NULL, NULL, '2022-09-08 10:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f548b781-2676-4322-8860-396940f79f2e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 07:00:00', '2022-09-08 07:00:00', 1, '2022-09-08 07:00:00', NULL, NULL, NULL, '2022-09-08 07:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f72df9d7-0905-40c2-9f6e-73d4079aa249', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 02:00:00', '2022-09-08 02:00:00', 1, '2022-09-08 02:00:00', NULL, NULL, NULL, '2022-09-08 02:00:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f7c34b46-b392-4978-99e3-1ebfe0820500', 'c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', 0, '2022-09-07 16:30:00', '2022-09-07 16:30:00', 1, '2022-09-07 16:30:00', NULL, NULL, NULL, '2022-09-07 16:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('f89a042c-e341-4f63-bb37-9d80eecfd15a', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 00:30:00', '2022-09-08 00:30:00', 1, '2022-09-08 00:30:00', NULL, NULL, NULL, '2022-09-08 00:30:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('fc63e23f-bf5a-4da7-8355-236f1c73d88e', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 10:15:00', '2022-09-08 10:15:01', 1, '2022-09-08 10:15:00', NULL, NULL, NULL, '2022-09-08 10:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('fda2b321-1792-410e-920b-fe13701236db', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 00:15:00', '2022-09-08 00:15:00', 1, '2022-09-08 00:15:00', NULL, NULL, NULL, '2022-09-08 00:15:00', NULL, NULL, NULL);
INSERT INTO `Sys_QuartzLog` VALUES ('ffb54070-c289-4907-822c-52d1d2ad0ae8', 'c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', 0, '2022-09-08 07:15:00', '2022-09-08 07:15:00', 1, '2022-09-08 07:15:00', NULL, NULL, NULL, '2022-09-08 07:15:00', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for Sys_QuartzOptions
-- ----------------------------
DROP TABLE IF EXISTS `Sys_QuartzOptions`;
CREATE TABLE `Sys_QuartzOptions`  (
  `Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TaskName` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `GroupName` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务分组',
  `CronExpression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Corn表达式',
  `Method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `ApiUrl` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Url地址',
  `AuthKey` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'AuthKey',
  `AuthValue` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'AuthValue',
  `LastRunTime` datetime(0) NULL DEFAULT NULL COMMENT '最后执行时间',
  `Status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `Describe` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `PostData` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'post参数',
  `TimeOut` int(11) NULL DEFAULT NULL COMMENT '超时时间(秒)',
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_QuartzOptions
-- ----------------------------
INSERT INTO `Sys_QuartzOptions` VALUES ('22f72d79-7f6f-4c0f-bf4f-c09c529ddc40', '每日统计', '订单管理', '0/0 10 0 * * ? *', 'get', 'https://www.baidu.com/', NULL, NULL, '2022-09-08 09:58:33', 0, '每天0:10执行一次	', ' ', 180, 1, '超级管理员', '2022-09-07 10:28:25', 1, '超级管理员', '2022-09-07 19:21:32');
INSERT INTO `Sys_QuartzOptions` VALUES ('c1745e19-3b39-4907-9e70-8319c9b831c8', '入库订单', '订单管理', '0 0/15 * * * ?', 'post', 'https://api.volcore.xyz/api/Sys_QuartzOptions/test', NULL, NULL, '2022-09-08 19:15:00', 0, '15分钟执行一次', '1', 180, 1, '超级管理员', '2022-09-07 10:27:02', 1, '超级管理员', '2022-09-07 13:44:34');
INSERT INTO `Sys_QuartzOptions` VALUES ('c9317a99-9802-4456-ae83-876a01306f3c', '订单合并', '订单管理', '0 0/30 * * * ?', 'post', 'https://api.volcore.xyz/api/Sys_QuartzOptions/test	', NULL, NULL, '2022-09-08 19:00:00', 0, '30分钟执行一次', '\n', 180, 1, '超级管理员', '2022-09-07 10:24:51', 1, '超级管理员', '2022-09-07 19:21:43');

-- ----------------------------
-- Table structure for Sys_Role
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Role`;
CREATE TABLE `Sys_Role`  (
  `Role_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DeleteBy` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DeptName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Dept_Id` int(11) NULL DEFAULT NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `ParentId` int(11) NULL DEFAULT NULL,
  `RoleName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`Role_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Role
-- ----------------------------
INSERT INTO `Sys_Role` VALUES (1, '2018-08-23 11:46:06.000000', '超级管理员', NULL, '无', 0, 1, '测试超级管理员', '2018-09-06 17:08:35.000000', 1000, 0, '超级管理员');
INSERT INTO `Sys_Role` VALUES (2, '2018-08-23 11:46:52.000000', '超级管理员', NULL, '1', 0, 1, '超级管理员', '2019-12-08 21:30:10.000000', NULL, 1, '测试管理员');
INSERT INTO `Sys_Role` VALUES (3, '2018-08-23 11:47:10.000000', '超级管理员', NULL, '无', 0, 0, '超级管理员', '2019-12-08 22:03:31.000000', NULL, 2, '小编x');
INSERT INTO `Sys_Role` VALUES (4, '2018-08-23 11:47:41.000000', '超级管理员', NULL, '无  ', 0, 1, '超级管理员', '2019-12-08 21:11:11.000000', NULL, 2, '信息员');
INSERT INTO `Sys_Role` VALUES (5, '2019-05-30 10:59:13.000000', '超级管理员', NULL, '还没想好', NULL, 1, '超级管理员', '2019-12-08 21:14:23.000000', NULL, 1, '主管');
INSERT INTO `Sys_Role` VALUES (7, '2019-11-20 18:00:18.000000', '超级管理员', NULL, NULL, NULL, 1, '超级管理员', '2019-12-08 21:29:38.000000', NULL, 1, '测试');
INSERT INTO `Sys_Role` VALUES (13, '2020-01-12 19:11:10.000000', '超级管理员', NULL, NULL, NULL, 1, '超级管理员', '2020-04-26 17:26:35.000000', NULL, 2, '测试1');
INSERT INTO `Sys_Role` VALUES (14, '2020-01-12 19:11:17.000000', '超级管理员', NULL, NULL, NULL, 1, '超级管理员', '2020-04-26 17:24:57.000000', NULL, 2, '测试2');
INSERT INTO `Sys_Role` VALUES (15, '2020-01-12 19:11:23.000000', '超级管理员', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, '测试3');
INSERT INTO `Sys_Role` VALUES (16, '2020-01-12 19:11:30.000000', '超级管理员', NULL, NULL, NULL, 1, '超级管理员', '2020-04-26 17:26:28.000000', NULL, 2, '测试4');
INSERT INTO `Sys_Role` VALUES (17, '2020-01-12 19:11:37.000000', '超级管理员', NULL, NULL, NULL, 1, '超级管理员', '2020-04-26 17:41:01.000000', NULL, 13, '测试5');

-- ----------------------------
-- Table structure for Sys_RoleAuthData
-- ----------------------------
DROP TABLE IF EXISTS `Sys_RoleAuthData`;
CREATE TABLE `Sys_RoleAuthData`  (
  `Auth_Id` int(11) NOT NULL AUTO_INCREMENT,
  `DataType_Id` int(11) NULL DEFAULT NULL,
  `Role_Id` int(11) NULL DEFAULT NULL,
  `User_Id` int(11) NULL DEFAULT NULL,
  `Node_Id` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `LevelID` int(11) NULL DEFAULT NULL,
  `AuthValue` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`Auth_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_RoleAuthData
-- ----------------------------

-- ----------------------------
-- Table structure for Sys_Roleauth
-- ----------------------------
DROP TABLE IF EXISTS `Sys_Roleauth`;
CREATE TABLE `Sys_Roleauth`  (
  `Auth_Id` int(11) NOT NULL AUTO_INCREMENT,
  `AuthValue` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Menu_Id` int(11) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `Role_Id` int(11) NULL DEFAULT NULL,
  `User_Id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Auth_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_Roleauth
-- ----------------------------
INSERT INTO `Sys_Roleauth` VALUES (1, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 9, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (2, 'Search,Add,Delete,Update,Import,Export', '2020-05-05 13:23:11.000000', '超级管理员', 53, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (3, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 50, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (4, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 40, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (5, 'Search,Add,Delete,Update,Export', '2020-05-05 13:23:11.000000', '超级管理员', 3, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (6, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 37, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (7, 'Search,Add,Delete,Update,Export,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 51, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (8, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 59, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (9, 'Search,Add,Delete,Update', '2020-05-05 13:23:11.000000', '超级管理员', 13, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (10, 'Search,Add,Delete,Update,Import,Export', '2020-05-05 13:23:11.000000', '超级管理员', 44, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (11, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 24, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (12, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 35, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (13, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 60, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (14, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 58, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (15, 'Search,Add,Delete,Update,Export,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 68, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (16, 'Search,Add,Delete,Update,Import,Export', '2020-05-05 13:23:11.000000', '超级管理员', 52, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (17, 'Search,Add,Delete,Update', '2020-05-05 13:23:11.000000', '超级管理员', 65, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (18, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 62, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (19, 'Search,Add,Delete,Update,Export', '2020-05-05 13:23:11.000000', '超级管理员', 63, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (20, 'Search,Add,Delete,Update,Export', '2020-05-05 13:23:11.000000', '超级管理员', 54, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (21, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 94, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (22, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 42, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (23, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 34, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (24, 'Search,Add,Delete,Update,Export', '2020-05-05 13:23:11.000000', '超级管理员', 90, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (25, 'Search,Add,Delete,Update,Export', '2020-05-05 13:23:11.000000', '超级管理员', 67, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (26, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 91, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (27, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 36, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (28, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 77, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (29, 'Search,Delete,Export', '2020-05-05 13:23:11.000000', '超级管理员', 6, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (30, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 88, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (31, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 61, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (32, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 8, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (33, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 48, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (34, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 74, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (35, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 56, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (36, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 55, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (37, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 32, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (38, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 33, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (39, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 92, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (40, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 89, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (41, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 86, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (42, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 84, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (43, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 29, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (44, 'Search,Add,Delete,Update,Import,Export', '2020-05-05 13:23:11.000000', '超级管理员', 31, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (45, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 72, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (46, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 66, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (47, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 28, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (48, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 64, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (49, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 5, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (50, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 25, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (51, 'Search,Add,Delete,Update', '2020-05-05 13:23:11.000000', '超级管理员', 93, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (52, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 85, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (53, 'Search,Add,Delete,Update,Import,Export,Upload,Audit', '2020-05-05 13:23:11.000000', '超级管理员', 75, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (54, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 87, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (55, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 57, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (56, 'Search,Export,test', '2020-05-05 13:23:11.000000', '超级管理员', 49, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (57, 'Search,Update', '2020-05-05 13:23:11.000000', '超级管理员', 71, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (58, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 2, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);
INSERT INTO `Sys_Roleauth` VALUES (59, 'Search', '2020-05-05 13:23:11.000000', '超级管理员', 73, '超级管理员', '2020-05-05 13:23:11.000000', 2, NULL);

-- ----------------------------
-- Table structure for Sys_TableColumn
-- ----------------------------
DROP TABLE IF EXISTS `Sys_TableColumn`;
CREATE TABLE `Sys_TableColumn`  (
  `ColumnId` int(11) NOT NULL AUTO_INCREMENT,
  `ApiInPut` int(11) NULL DEFAULT NULL,
  `ApiIsNull` int(11) NULL DEFAULT NULL,
  `ApiOutPut` int(11) NULL DEFAULT NULL,
  `ColSize` int(11) NULL DEFAULT NULL,
  `ColumnCNName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ColumnName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ColumnType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ColumnWidth` int(11) NULL DEFAULT NULL,
  `Columnformat` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DropNo` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `EditColNo` int(11) NULL DEFAULT NULL,
  `EditRowNo` int(11) NULL DEFAULT NULL,
  `EditType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `IsColumnData` int(11) NULL DEFAULT NULL,
  `IsDisplay` int(11) NULL DEFAULT NULL,
  `IsImage` int(11) NULL DEFAULT NULL,
  `IsKey` int(11) NULL DEFAULT NULL,
  `IsNull` int(11) NULL DEFAULT NULL,
  `IsReadDataset` int(11) NULL DEFAULT NULL,
  `Maxlength` int(11) NULL DEFAULT NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `Script` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `SearchColNo` int(11) NULL DEFAULT NULL,
  `SearchRowNo` int(11) NULL DEFAULT NULL,
  `SearchType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Sortable` int(11) NULL DEFAULT NULL,
  `TableName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Table_Id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ColumnId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 801 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_TableColumn
-- ----------------------------
INSERT INTO `Sys_TableColumn` VALUES (20, NULL, NULL, NULL, NULL, '角色ID', 'Role_Id', 'int', 70, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 0, 1, 4, '超级管理员', '2023-02-22 00:32:08.774318', 1, 1420, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (21, NULL, NULL, NULL, NULL, '父级ID', 'ParentId', 'int', 70, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 4, '超级管理员', '2023-02-22 00:32:08.774504', 1, 1410, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (22, NULL, NULL, NULL, NULL, '角色名称', 'RoleName', 'string', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 100, '超级管理员', '2023-02-22 00:32:08.774533', 1, 1400, NULL, NULL, 1, 'text', 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (23, NULL, NULL, NULL, NULL, '部门ID', 'Dept_Id', 'int', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:32:08.774558', 1, 1390, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (24, NULL, NULL, NULL, NULL, '部门名称', 'DeptName', 'string', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:32:08.774597', 1, 1380, NULL, NULL, 1, 'text', 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (25, NULL, NULL, NULL, NULL, '排序', 'OrderNo', 'int', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:32:08.774649', 1, 1370, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (26, NULL, NULL, NULL, NULL, '创建人', 'Creator', 'string', 130, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, 1, NULL, 0, 1, 1, 100, '超级管理员', '2023-02-22 00:32:08.774672', 1, 1360, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (27, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 4, 'datetime', NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2023-02-22 00:32:08.774720', 1, 1350, NULL, NULL, 2, 'datetime', 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (28, NULL, NULL, NULL, NULL, '修改人', 'Modifier', 'string', 130, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, 1, NULL, 0, 1, 1, 100, '超级管理员', '2023-02-22 00:32:08.774743', 1, 1340, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (29, NULL, NULL, NULL, NULL, '修改时间', 'ModifyDate', 'DateTime', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, 5, NULL, NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2023-02-22 00:32:08.774765', 1, 1330, NULL, NULL, 2, 'datetime', 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (30, NULL, NULL, NULL, NULL, NULL, 'DeleteBy', 'string', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:32:08.774790', 1, 1320, NULL, NULL, NULL, NULL, 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (31, NULL, NULL, NULL, NULL, '是否启用', 'Enable', 'byte', 90, NULL, '2018-06-04 10:14:21.000000', NULL, NULL, 'enable', NULL, 2, 'switch', NULL, 1, 1, NULL, 0, 1, 0, 1, '超级管理员', '2023-02-22 00:32:08.774623', 1, 1375, NULL, NULL, 1, 'select', 0, 'Sys_Role', 2);
INSERT INTO `Sys_TableColumn` VALUES (32, NULL, NULL, NULL, NULL, '字典ID', 'Dic_ID', 'int', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 4, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1300, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (33, NULL, NULL, NULL, NULL, '字典名称', 'DicName', 'string', 140, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 200, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1290, NULL, NULL, 1, 'textarea', NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (34, NULL, NULL, NULL, NULL, '父级ID', 'ParentId', 'int', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 4, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1280, NULL, NULL, 1, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (35, NULL, NULL, NULL, NULL, '配置项', 'Config', 'string', 300, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8000, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1270, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (36, NULL, NULL, NULL, 8, 'sql语句', 'DbSql', 'string', 200, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 6, 'textarea', NULL, 1, 1, NULL, 0, 1, 0, 8000, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1260, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (37, NULL, NULL, NULL, NULL, 'DBServer', 'DBServer', 'string', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8000, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1250, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (38, NULL, NULL, NULL, NULL, '排序号', 'OrderNo', 'int', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 2, NULL, NULL, 1, 1, NULL, 0, 1, 0, 4, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1240, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (39, NULL, NULL, NULL, NULL, '字典编号', 'DicNo', 'string', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 200, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1295, NULL, NULL, 1, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (40, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 6, 'textarea', NULL, 1, 1, NULL, 0, 1, 0, 4000, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1220, NULL, NULL, NULL, '无', NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (41, NULL, NULL, NULL, NULL, '是否启用', 'Enable', 'byte', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, 'enable', NULL, 2, 'select', NULL, 1, 1, NULL, 0, 0, 0, 1, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1210, NULL, NULL, 2, 'select', NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (42, NULL, NULL, NULL, NULL, NULL, 'CreateID', 'int', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1200, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (43, NULL, NULL, NULL, NULL, '创建人', 'Creator', 'string', 130, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 1, 60, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1190, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (44, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 150, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, 2, 'datetime', NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1180, NULL, NULL, 2, 'datetime', NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (45, NULL, NULL, NULL, NULL, NULL, 'ModifyID', 'int', 90, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1170, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (46, NULL, NULL, NULL, NULL, '修改人', 'Modifier', 'string', 130, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 1, 60, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1160, NULL, NULL, NULL, NULL, NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (47, NULL, NULL, NULL, NULL, '修改时间', 'ModifyDate', 'DateTime', 150, NULL, '2018-06-06 14:05:43.000000', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2020-04-25 11:15:33.000000', 1, 1150, NULL, NULL, 2, 'datetime', NULL, 'Sys_Dictionary', 3);
INSERT INTO `Sys_TableColumn` VALUES (48, NULL, NULL, NULL, NULL, NULL, 'DicList_ID', 'int', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 4, '超级管理员', '2023-02-22 00:36:17.668503', 1, 1140, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (49, NULL, NULL, NULL, NULL, '数据源ID', 'Dic_ID', 'int', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 1, NULL, 0, 1, 1, 4, '超级管理员', '2023-02-22 00:36:17.668658', 1, 1130, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (50, NULL, NULL, NULL, NULL, '数据源Value', 'DicValue', 'string', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, 1, 'text', NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:36:17.668685', 1, 1120, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (51, NULL, NULL, NULL, NULL, '数据源Text', 'DicName', 'string', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, 1, 'text', NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:36:17.668710', 1, 1110, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (52, NULL, NULL, NULL, NULL, '排序号', 'OrderNo', 'int', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, 1, 'text', NULL, 1, 1, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:17.668737', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (53, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, 1, 'text', NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:36:17.668760', 1, 1090, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (54, NULL, NULL, NULL, NULL, '是否可用', 'Enable', 'byte', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, 'enable', NULL, 1, 'switch', NULL, 1, 1, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:17.668783', 1, 1080, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (55, NULL, NULL, NULL, NULL, NULL, 'CreateID', 'int', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, '无', NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:17.668806', 1, 1070, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (56, NULL, NULL, NULL, NULL, '创建人', 'Creator', 'string', 130, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 1, 30, '超级管理员', '2023-02-22 00:36:17.668848', 1, 1060, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (57, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2023-02-22 00:36:17.668900', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (58, NULL, NULL, NULL, NULL, NULL, 'ModifyID', 'int', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:17.668928', 1, 1040, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (59, NULL, NULL, NULL, NULL, '修改人', 'Modifier', 'string', 130, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:36:17.668953', 1, 1030, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (60, NULL, NULL, NULL, NULL, '修改时间', 'ModifyDate', 'DateTime', 90, NULL, '2018-06-06 14:12:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:17.668976', 1, 1020, NULL, NULL, NULL, NULL, 0, 'Sys_DictionaryList', 4);
INSERT INTO `Sys_TableColumn` VALUES (61, NULL, NULL, NULL, NULL, NULL, 'Id', 'int', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 4, '超级管理员', '2023-02-22 00:36:11.973780', 1, 10000, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (62, NULL, NULL, NULL, 12, '日志类型', 'LogType', 'string', 80, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, 'log', NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:36:11.974005', 1, 8888, NULL, NULL, 3, 'checkbox', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (63, NULL, NULL, NULL, NULL, '请求参数', 'RequestParameter', 'string', 70, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 20000, '超级管理员', '2023-02-22 00:36:11.974078', 1, 7990, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (64, NULL, NULL, NULL, NULL, '响应参数', 'ResponseParameter', 'string', 70, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 20000, '超级管理员', '2023-02-22 00:36:11.974102', 1, 7980, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (65, NULL, NULL, NULL, NULL, '异常信息', 'ExceptionInfo', 'string', 70, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 20000, '超级管理员', '2023-02-22 00:36:11.974125', 1, 7970, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (66, NULL, NULL, NULL, NULL, '响应状态', 'Success', 'int', 80, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, 'restatus', NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:11.974029', 1, 8700, NULL, NULL, 2, 'selectList', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (67, NULL, NULL, NULL, NULL, '开始时间', 'BeginDate', 'DateTime', 140, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:11.973925', 1, 9999, NULL, NULL, 2, 'datetime', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (68, NULL, NULL, NULL, NULL, '结束时间', 'EndDate', 'DateTime', 150, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:11.974261', 1, 880, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (69, NULL, NULL, NULL, NULL, '时长', 'ElapsedTime', 'int', 60, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:11.974054', 1, 8600, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (70, NULL, NULL, NULL, NULL, '用户IP', 'UserIP', 'string', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:36:11.974149', 1, 7920, NULL, NULL, 1, 'text', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (71, NULL, NULL, NULL, NULL, '服务器IP', 'ServiceIP', 'string', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:36:11.974172', 1, 7910, NULL, NULL, 1, 'text', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (72, NULL, NULL, NULL, NULL, '浏览器类型', 'BrowserType', 'string', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 400, '超级管理员', '2023-02-22 00:36:11.974197', 1, 7900, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (73, NULL, NULL, NULL, NULL, '请求地址', 'Url', 'string', 110, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8000, '超级管理员', '2023-02-22 00:36:11.973978', 1, 9000, NULL, NULL, 1, 'text', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (74, NULL, NULL, NULL, NULL, '用户ID', 'User_Id', 'int', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:11.974220', 1, 7880, NULL, NULL, NULL, 'text', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (75, NULL, NULL, NULL, NULL, '用户名称', 'UserName', 'string', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8000, '超级管理员', '2023-02-22 00:36:11.973953', 1, 9100, NULL, NULL, NULL, NULL, 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (76, NULL, NULL, NULL, NULL, '角色ID', 'Role_Id', 'int', 90, NULL, '2018-06-11 18:22:16.000000', NULL, NULL, 'roles', NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:11.974243', 1, 7860, NULL, NULL, 2, 'select', 0, 'Sys_Log', 5);
INSERT INTO `Sys_TableColumn` VALUES (77, NULL, NULL, NULL, NULL, NULL, 'User_Id', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 4, '超级管理员', '2023-02-22 00:36:04.547429', 1, 7850, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (78, NULL, NULL, NULL, NULL, NULL, 'Dept_Id', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.547523', 1, 7840, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (79, 0, NULL, 1, NULL, '部门', 'DeptName', 'string', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, 'text', NULL, 1, 0, NULL, 0, 1, 0, 300, '超级管理员', '2023-02-22 00:36:04.547550', 1, 7830, NULL, NULL, 2, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (80, 0, 0, 1, NULL, '角色', 'Role_Id', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, 'roles', NULL, 2, 'select', NULL, 1, 1, NULL, 0, 0, 0, 4, '超级管理员', '2023-02-22 00:36:04.547584', 1, 7820, NULL, NULL, 2, 'select', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (81, NULL, NULL, NULL, NULL, NULL, 'RoleName', 'string', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, 0, 0, 0, 300, '超级管理员', '2023-02-22 00:36:04.547617', 1, 7810, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (82, 1, 0, 1, NULL, '帐号', 'UserName', 'string', 100, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, NULL, 0, 0, 1, 200, '超级管理员', '2023-02-22 00:36:04.547260', 1, 7945, NULL, NULL, 1, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (83, 1, 0, NULL, NULL, '密码', 'UserPwd', 'string', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 1, 0, 400, '超级管理员', '2023-02-22 00:36:04.547727', 1, 7790, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (84, NULL, NULL, NULL, NULL, '真实姓名', 'UserTrueName', 'string', 100, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 1, 'text', NULL, 1, 1, NULL, 0, 0, 0, 40, '超级管理员', '2023-02-22 00:36:04.547700', 1, 7792, NULL, NULL, 1, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (85, NULL, NULL, NULL, NULL, '地址', 'Address', 'string', 190, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, 'text', NULL, 1, 0, NULL, 0, 1, 0, 400, '超级管理员', '2023-02-22 00:36:04.548157', 1, 7270, NULL, NULL, 4, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (86, NULL, NULL, NULL, NULL, '电话', 'Mobile', 'string', 140, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, 'text', NULL, 1, 0, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:36:04.548178', 1, 7260, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (87, NULL, NULL, NULL, NULL, 'Email', 'Email', 'string', 140, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, 'mail', NULL, 1, 0, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:36:04.548198', 1, 7250, NULL, NULL, 4, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (88, NULL, NULL, NULL, NULL, NULL, 'Tel', 'string', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 40, '超级管理员', '2023-02-22 00:36:04.547856', 1, 7740, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (89, NULL, NULL, NULL, 12, '备注', 'Remark', 'string', 180, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 7, 'textarea', NULL, 1, 0, NULL, 0, 1, 0, 400, '超级管理员', '2023-02-22 00:36:04.548219', 1, 7230, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (90, NULL, NULL, NULL, NULL, '排序号', 'OrderNo', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 0, 'text', NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.548240', 1, 7220, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (91, NULL, NULL, NULL, NULL, '是否可用', 'Enable', 'byte', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, 'enable', NULL, 4, 'select', NULL, 1, 1, NULL, 0, 0, 0, 1, '超级管理员', '2023-02-22 00:36:04.547945', 1, 7670, NULL, NULL, 4, 'select', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (92, NULL, NULL, NULL, NULL, NULL, 'CreateID', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.547883', 1, 7700, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (93, NULL, NULL, NULL, NULL, '创建人', 'Creator', 'string', 100, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, 1, NULL, 0, 1, 1, 400, '超级管理员', '2023-02-22 00:36:04.547920', 1, 7690, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (94, NULL, NULL, NULL, NULL, '注册时间', 'CreateDate', 'DateTime', 150, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, 6, NULL, NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2023-02-22 00:36:04.547766', 1, 7780, NULL, NULL, 5, 'datetime', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (95, NULL, NULL, NULL, NULL, NULL, 'ModifyID', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.547968', 1, 7670, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (96, NULL, NULL, NULL, NULL, '修改人', 'Modifier', 'string', 130, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 1, 400, '超级管理员', '2023-02-22 00:36:04.547990', 1, 7660, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (97, NULL, NULL, NULL, NULL, '修改时间', 'ModifyDate', 'DateTime', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 1, 1, NULL, 0, 1, 1, 8, '超级管理员', '2023-02-22 00:36:04.548011', 1, 7650, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (98, NULL, NULL, NULL, NULL, '审核状态', 'AuditStatus', 'int', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, 'audit', NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.548041', 1, 7640, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (99, NULL, NULL, NULL, NULL, '审核人', 'Auditor', 'string', 90, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 400, '超级管理员', '2023-02-22 00:36:04.548062', 1, 7630, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (100, NULL, NULL, NULL, NULL, '审核时间', 'AuditDate', 'DateTime', 150, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:04.548083', 1, 7620, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (101, NULL, NULL, NULL, NULL, '最后登陆时间', 'LastLoginDate', 'DateTime', 150, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:04.548115', 1, 7610, NULL, NULL, 5, 'datetime', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (102, NULL, NULL, NULL, NULL, '最后密码修改时间', 'LastModifyPwdDate', 'DateTime', 150, NULL, '2018-06-14 16:44:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8, '超级管理员', '2023-02-22 00:36:04.548137', 1, 7600, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (114, NULL, NULL, NULL, NULL, '头像', 'HeadImageUrl', 'string', 90, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'img', NULL, 1, 1, 1, 0, 0, 0, 400, '超级管理员', '2023-02-22 00:36:04.547493', 1, 7842, NULL, NULL, NULL, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (429, 1, 0, NULL, NULL, '手机号', 'PhoneNo', 'string', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 22, '超级管理员', '2023-02-22 00:36:04.547830', 1, 7760, NULL, NULL, 3, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (431, NULL, NULL, NULL, NULL, 'Token', 'Token', 'string', 180, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, NULL, 1, 0, 1000, '超级管理员', '2023-02-22 00:36:04.547645', 1, 7810, NULL, NULL, 2, NULL, 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (506, NULL, NULL, NULL, NULL, '性别', 'Gender', 'int', 100, NULL, NULL, NULL, NULL, 'gender', NULL, 4, 'select', NULL, 1, 1, NULL, NULL, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.547464', 1, 7843, NULL, NULL, 1, 'select', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (654, NULL, NULL, NULL, NULL, '登陆设备类型', 'AppType', 'int', 150, NULL, NULL, NULL, NULL, 'ut', NULL, 0, NULL, NULL, 1, 0, NULL, NULL, 1, 0, 4, '超级管理员', '2023-02-22 00:36:04.547673', 1, 7809, NULL, NULL, 3, 'selectList', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (659, NULL, NULL, NULL, NULL, '手机用户', 'IsRegregisterPhone', 'int', 120, NULL, NULL, NULL, NULL, 'isphone', NULL, 2, 'select', NULL, 1, 0, NULL, NULL, 0, 0, 4, '超级管理员', '2023-02-22 00:36:04.547803', 1, 7771, NULL, NULL, 3, 'select', 0, 'Sys_User', 6);
INSERT INTO `Sys_TableColumn` VALUES (660, NULL, NULL, NULL, NULL, NULL, 'Order_Id', 'guid', 110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 16, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (661, NULL, NULL, NULL, NULL, '单号', 'ReportTranNo', 'string', 110, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 100, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, 1, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (662, NULL, NULL, NULL, NULL, '订单', 'ReportSellNo', 'string', 220, NULL, NULL, NULL, NULL, NULL, 0, 2, NULL, NULL, 1, 1, NULL, 0, 0, 0, 255, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, 1, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (663, NULL, NULL, NULL, NULL, '数量', 'ReportQty', 'int', 80, NULL, NULL, NULL, NULL, NULL, 0, 3, NULL, NULL, 1, 1, NULL, 0, 0, 0, 4, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, 1, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (664, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 220, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 1, 1, NULL, 0, 1, 0, 1000, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (665, NULL, NULL, NULL, NULL, NULL, 'CreateID', 'int', 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (666, NULL, NULL, NULL, NULL, NULL, 'Creator', 'string', 130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 255, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (667, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (668, NULL, NULL, NULL, NULL, NULL, 'ModifyID', 'int', 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (669, NULL, NULL, NULL, NULL, NULL, 'Modifier', 'string', 130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 255, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (670, NULL, NULL, NULL, NULL, '修改时间', 'ModifyDate', 'DateTime', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8, '超级管理员', '2022-05-04 11:18:41.000000', 1, 0, NULL, NULL, NULL, NULL, 0, 'ReportDBTest', 12);
INSERT INTO `Sys_TableColumn` VALUES (671, NULL, NULL, NULL, NULL, NULL, 'Order_Id', 'guid', 110, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 16, '超级管理员', '2022-05-04 11:34:18.000000', 1, 1050, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (672, NULL, NULL, NULL, NULL, '订单号', 'ServiceTranNo', 'string', 120, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, 0, 1, NULL, NULL, 1, 1, NULL, 0, 0, 0, 100, '超级管理员', '2022-05-04 11:34:18.000000', 1, 1000, NULL, NULL, 1, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (673, NULL, NULL, NULL, NULL, '运单号', 'ServiceSellNo', 'string', 220, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, 0, 2, NULL, NULL, 1, 1, NULL, 0, 0, 0, 255, '超级管理员', '2022-05-04 11:34:18.000000', 1, 950, NULL, NULL, 1, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (674, NULL, NULL, NULL, NULL, '数量', 'ServiceQty', 'int', 110, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, 0, 3, NULL, NULL, 1, 1, NULL, 0, 0, 0, 4, '超级管理员', '2022-05-04 11:34:18.000000', 1, 900, NULL, NULL, 1, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (675, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 220, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, 4, NULL, NULL, 1, 1, NULL, 0, 1, 0, 1000, '超级管理员', '2022-05-04 11:34:18.000000', 1, 850, NULL, NULL, 1, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (676, NULL, NULL, NULL, NULL, NULL, 'CreateID', 'int', 80, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2022-05-04 11:34:18.000000', 1, 800, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (677, NULL, NULL, NULL, NULL, '创建人', 'Creator', 'string', 130, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 255, '超级管理员', '2022-05-04 11:34:18.000000', 1, 750, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (678, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 110, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 8, '超级管理员', '2022-05-04 11:34:18.000000', 1, 700, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (679, NULL, NULL, NULL, NULL, NULL, 'ModifyID', 'int', 80, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 4, '超级管理员', '2022-05-04 11:34:18.000000', 1, 650, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (680, NULL, NULL, NULL, NULL, NULL, 'Modifier', 'string', 130, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 255, '超级管理员', '2022-05-04 11:34:18.000000', 1, 600, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (681, NULL, NULL, NULL, NULL, NULL, 'ModifyDate', 'DateTime', 110, NULL, '2022-05-04 11:32:35.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, 8, '超级管理员', '2022-05-04 11:34:18.000000', 1, 550, NULL, NULL, NULL, NULL, 0, 'ServiceDbTest', 14);
INSERT INTO `Sys_TableColumn` VALUES (682, NULL, NULL, NULL, NULL, '', 'Id', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:43:29.283618', 1, 2050, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (683, NULL, NULL, NULL, NULL, '描述', 'Describe', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 0, '超级管理员', '2023-02-22 00:43:29.285551', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (684, NULL, NULL, NULL, NULL, '状态', 'Status', 'int', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285517', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (685, NULL, NULL, NULL, NULL, '最后执行时间', 'LastRunTime', 'DateTime', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285487', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (686, NULL, NULL, NULL, NULL, 'AuthValue', 'AuthValue', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.285455', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (687, NULL, NULL, NULL, NULL, 'AuthKey', 'AuthKey', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.285425', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (688, NULL, NULL, NULL, NULL, 'Url地址', 'ApiUrl', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.285391', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (689, NULL, NULL, NULL, NULL, '请求方式', 'Method', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 50, '超级管理员', '2023-02-22 00:43:29.285359', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (690, NULL, NULL, NULL, NULL, 'Corn表达式', 'CronExpression', 'string', 120, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 100, '超级管理员', '2023-02-22 00:43:29.285324', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (691, NULL, NULL, NULL, NULL, '任务分组', 'GroupName', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 500, '超级管理员', '2023-02-22 00:43:29.285285', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (692, NULL, NULL, NULL, NULL, '任务名称', 'TaskName', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 500, '超级管理员', '2023-02-22 00:43:29.285254', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (693, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285216', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (694, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:43:29.285182', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (695, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285152', 1, 1250, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (696, NULL, NULL, NULL, NULL, 'post参数', 'PostData', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 0, '超级管理员', '2023-02-22 00:43:29.285594', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (697, NULL, NULL, NULL, NULL, '', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285120', 1, 1300, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (698, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285056', 1, 1400, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (699, NULL, NULL, NULL, NULL, '', 'TimeOut', 'int', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285026', 1, 1450, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (700, NULL, NULL, NULL, NULL, '', 'PostData', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 0, '超级管理员', '2023-02-22 00:43:29.284991', 1, 1500, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (701, NULL, NULL, NULL, NULL, '', 'Describe', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 0, '超级管理员', '2023-02-22 00:43:29.284960', 1, 1550, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (702, NULL, NULL, NULL, NULL, '', 'Status', 'int', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.284929', 1, 1600, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (703, NULL, NULL, NULL, NULL, '', 'LastRunTime', 'DateTime', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.284897', 1, 1650, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (704, NULL, NULL, NULL, NULL, '', 'AuthValue', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.284863', 1, 1700, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (705, NULL, NULL, NULL, NULL, '', 'AuthKey', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.284828', 1, 1750, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (706, NULL, NULL, NULL, NULL, '', 'ApiUrl', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 2000, '超级管理员', '2023-02-22 00:43:29.284793', 1, 1800, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (707, NULL, NULL, NULL, NULL, '', 'Method', 'string', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 50, '超级管理员', '2023-02-22 00:43:29.284758', 1, 1850, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (708, NULL, NULL, NULL, NULL, '', 'CronExpression', 'string', 120, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 100, '超级管理员', '2023-02-22 00:43:29.284719', 1, 1900, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (709, NULL, NULL, NULL, NULL, '', 'GroupName', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 500, '超级管理员', '2023-02-22 00:43:29.284680', 1, 1950, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (710, NULL, NULL, NULL, NULL, '', 'TaskName', 'string', 220, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 500, '超级管理员', '2023-02-22 00:43:29.284616', 1, 2000, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (711, NULL, NULL, NULL, NULL, '', 'Creator', 'string', 130, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:43:29.285088', 1, 1350, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (712, NULL, NULL, NULL, NULL, '超时时间(秒)', 'TimeOut', 'int', 110, NULL, '2023-02-22 00:43:18.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:29.285625', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzOptions', 16);
INSERT INTO `Sys_TableColumn` VALUES (713, NULL, NULL, NULL, NULL, '', 'LogId', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:43:57.728153', 1, 1600, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (714, NULL, NULL, NULL, NULL, '是否成功', 'Result', 'int', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.729158', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (715, NULL, NULL, NULL, NULL, '结束时间', 'EndDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.729105', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (716, NULL, NULL, NULL, NULL, '开始时间', 'StratDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.729078', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (717, NULL, NULL, NULL, NULL, '耗时(秒)', 'ElapsedTime', 'int', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.729047', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (718, NULL, NULL, NULL, NULL, '任务名称', 'TaskName', 'string', 220, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 500, '超级管理员', '2023-02-22 00:43:57.729018', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (719, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728991', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (720, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:43:57.728957', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (721, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728909', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (722, NULL, NULL, NULL, NULL, '', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728861', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (723, NULL, NULL, NULL, NULL, '', 'Creator', 'string', 130, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:43:57.728809', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (724, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728766', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (725, NULL, NULL, NULL, NULL, '', 'ErrorMsg', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:43:57.728720', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (726, NULL, NULL, NULL, NULL, '', 'ResponseContent', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:43:57.728678', 1, 1250, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (727, NULL, NULL, NULL, NULL, '', 'Result', 'int', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728636', 1, 1300, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (728, NULL, NULL, NULL, NULL, '', 'EndDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728594', 1, 1350, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (729, NULL, NULL, NULL, NULL, '', 'StratDate', 'DateTime', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728546', 1, 1400, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (730, NULL, NULL, NULL, NULL, '', 'ElapsedTime', 'int', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:43:57.728497', 1, 1450, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (731, NULL, NULL, NULL, NULL, '', 'TaskName', 'string', 220, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 500, '超级管理员', '2023-02-22 00:43:57.728450', 1, 1500, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (732, NULL, NULL, NULL, NULL, '', 'Id', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 36, '超级管理员', '2023-02-22 00:43:57.728387', 1, 1550, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (733, NULL, NULL, NULL, NULL, '返回内容', 'ResponseContent', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:43:57.729185', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (734, NULL, NULL, NULL, NULL, '异常信息', 'ErrorMsg', 'string', 110, NULL, '2023-02-22 00:43:49.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:43:57.729212', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_QuartzLog', 17);
INSERT INTO `Sys_TableColumn` VALUES (735, NULL, NULL, NULL, NULL, '', 'Sys_WorkFlowTableStep_Id', 'string', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:44:46.566109', 1, 1550, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (736, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567235', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (737, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:44:46.567207', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (738, NULL, NULL, NULL, NULL, '', 'Enable', 'sbyte', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567181', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (739, NULL, NULL, NULL, NULL, '', 'Creator', 'string', 130, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:44:46.567155', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (740, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567126', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (741, NULL, NULL, NULL, NULL, '', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567100', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (742, NULL, NULL, NULL, NULL, '', 'Remark', 'string', 220, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 500, '超级管理员', '2023-02-22 00:44:46.567071', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (743, NULL, NULL, NULL, NULL, '审核时间', 'AuditDate', 'DateTime', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567043', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (744, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567266', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (745, NULL, NULL, NULL, NULL, '审核状态', 'AuditStatus', 'int', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.567011', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (746, NULL, NULL, NULL, NULL, '审核人id', 'AuditId', 'int', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.566958', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (747, NULL, NULL, NULL, NULL, '审批顺序', 'OrderId', 'int', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.566881', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (748, NULL, NULL, NULL, NULL, '节点类型(1=按用户审批,2=按角色审批,3=按部门审批 )', 'StepValue', 'int', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.566797', 1, 1250, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (749, NULL, NULL, NULL, NULL, '审批类型', 'StepType', 'int', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:44:46.566718', 1, 1300, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (750, NULL, NULL, NULL, NULL, '节名称', 'StepName', 'string', 180, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:44:46.566588', 1, 1350, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (751, NULL, NULL, NULL, NULL, '节点id', 'StepId', 'string', 120, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:44:46.566470', 1, 1400, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (752, NULL, NULL, NULL, NULL, '流程id', 'WorkFlow_Id', 'string', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 36, '超级管理员', '2023-02-22 00:44:46.566375', 1, 1450, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (753, NULL, NULL, NULL, NULL, '主表id', 'WorkFlowTable_Id', 'string', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 36, '超级管理员', '2023-02-22 00:44:46.566314', 1, 1500, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (754, NULL, NULL, NULL, NULL, '审核人', 'Auditor', 'string', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 50, '超级管理员', '2023-02-22 00:44:46.566985', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (755, NULL, NULL, NULL, NULL, '', 'Sys_WorkFlowTableStep_Id', 'string', 110, NULL, '2023-02-22 00:44:39.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 36, '超级管理员', '2023-02-22 00:44:46.567295', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTableStep', 19);
INSERT INTO `Sys_TableColumn` VALUES (756, NULL, NULL, NULL, NULL, '', 'WorkFlowTable_Id', 'string', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:45:12.376922', 1, 1300, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (757, NULL, NULL, NULL, NULL, '流程id', 'WorkFlow_Id', 'string', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 36, '超级管理员', '2023-02-22 00:45:12.377109', 1, 1250, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (758, NULL, NULL, NULL, NULL, '流程名称', 'WorkName', 'string', 180, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:12.377157', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (759, NULL, NULL, NULL, NULL, '表主键id', 'WorkTableKey', 'string', 180, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:12.377188', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (760, NULL, NULL, NULL, NULL, '表名', 'WorkTable', 'string', 180, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:12.377221', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (761, NULL, NULL, NULL, NULL, '业务名称', 'WorkTableName', 'string', 180, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:12.377250', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (762, NULL, NULL, NULL, NULL, '当前审批节点', 'CurrentOrderId', 'int', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377290', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (763, NULL, NULL, NULL, NULL, '审批状态', 'AuditStatus', 'int', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377331', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (764, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377360', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (765, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377386', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (766, NULL, NULL, NULL, NULL, '创建时间', 'Creator', 'string', 130, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:12.377412', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (767, NULL, NULL, NULL, NULL, '', 'Enable', 'sbyte', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377438', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (768, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:12.377470', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (769, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377495', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (770, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:12.377518', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (771, NULL, NULL, NULL, NULL, '', 'WorkFlowTable_Id', 'string', 110, NULL, '2023-02-22 00:45:06.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 36, '超级管理员', '2023-02-22 00:45:12.377541', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowTable', 20);
INSERT INTO `Sys_TableColumn` VALUES (772, NULL, NULL, NULL, NULL, '', 'WorkStepFlow_Id', 'string', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:45:33.724647', 1, 1250, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (773, NULL, NULL, NULL, NULL, '流程主表id', 'WorkFlow_Id', 'string', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 36, '超级管理员', '2023-02-22 00:45:33.724988', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (774, NULL, NULL, NULL, NULL, '流程节点Id', 'StepId', 'string', 120, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 100, '超级管理员', '2023-02-22 00:45:33.725047', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (775, NULL, NULL, NULL, NULL, '节点名称', 'StepName', 'string', 180, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:33.725106', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (776, NULL, NULL, NULL, NULL, '节点类型(1=按用户审批,2=按角色审批,3=按部门审批)', 'StepType', 'int', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725176', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (777, NULL, NULL, NULL, NULL, '审批用户id或角色id、部门id', 'StepValue', 'int', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725223', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (778, NULL, NULL, NULL, NULL, '审批顺序', 'OrderId', 'int', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725302', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (779, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 220, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 500, '超级管理员', '2023-02-22 00:45:33.725355', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (780, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725412', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (781, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725471', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (782, NULL, NULL, NULL, NULL, '', 'Creator', 'string', 130, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:33.725560', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (783, NULL, NULL, NULL, NULL, '', 'Enable', 'sbyte', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725682', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (784, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:33.725766', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (785, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725846', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (786, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:45:29.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:33.725898', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlowStep', 21);
INSERT INTO `Sys_TableColumn` VALUES (787, NULL, NULL, NULL, NULL, '', 'WorkFlow_Id', 'string', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 0, 1, 36, '超级管理员', '2023-02-22 00:45:53.353726', 1, 1200, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (788, NULL, NULL, NULL, NULL, '流程名称', 'WorkName', 'string', 180, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 200, '超级管理员', '2023-02-22 00:45:53.353861', 1, 1150, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (789, NULL, NULL, NULL, NULL, '表名', 'WorkTable', 'string', 180, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 0, 0, 200, '超级管理员', '2023-02-22 00:45:53.353886', 1, 1100, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (790, NULL, NULL, NULL, NULL, '功能菜单', 'WorkTableName', 'string', 180, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 200, '超级管理员', '2023-02-22 00:45:53.353914', 1, 1050, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (791, NULL, NULL, NULL, NULL, '节点信息', 'NodeConfig', 'string', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:45:53.353939', 1, 1000, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (792, NULL, NULL, NULL, NULL, '连接配置', 'LineConfig', 'string', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 0, '超级管理员', '2023-02-22 00:45:53.353960', 1, 950, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (793, NULL, NULL, NULL, NULL, '备注', 'Remark', 'string', 220, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 500, '超级管理员', '2023-02-22 00:45:53.353983', 1, 900, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (794, NULL, NULL, NULL, NULL, '创建时间', 'CreateDate', 'DateTime', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:53.354004', 1, 850, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (795, NULL, NULL, NULL, NULL, '', 'CreateID', 'int', 80, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:53.354026', 1, 800, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (796, NULL, NULL, NULL, NULL, '', 'Creator', 'string', 130, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:53.354046', 1, 750, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (797, NULL, NULL, NULL, NULL, '是否启用', 'Enable', 'sbyte', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:53.354069', 1, 700, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (798, NULL, NULL, NULL, NULL, '', 'Modifier', 'string', 130, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, 30, '超级管理员', '2023-02-22 00:45:53.354090', 1, 650, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (799, NULL, NULL, NULL, NULL, '', 'ModifyDate', 'DateTime', 110, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:53.354114', 1, 600, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);
INSERT INTO `Sys_TableColumn` VALUES (800, NULL, NULL, NULL, NULL, '', 'ModifyID', 'int', 80, NULL, '2023-02-22 00:45:47.000000', 1, '超级管理员', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 1, 0, NULL, '超级管理员', '2023-02-22 00:45:53.354135', 1, 550, NULL, NULL, NULL, NULL, 0, 'Sys_WorkFlow', 22);

-- ----------------------------
-- Table structure for Sys_TableInfo
-- ----------------------------
DROP TABLE IF EXISTS `Sys_TableInfo`;
CREATE TABLE `Sys_TableInfo`  (
  `Table_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CnName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ColumnCNName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DBServer` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DataTableType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DetailCnName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DetailName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `EditorType` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `ExpressField` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `FolderName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Namespace` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `ParentId` int(11) NULL DEFAULT NULL,
  `RichText` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `SortName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `TableName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `TableTrueName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UploadField` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UploadMaxCount` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Table_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_TableInfo
-- ----------------------------
INSERT INTO `Sys_TableInfo` VALUES (2, '角色管理', '角色管理', 'SysDbContext', NULL, NULL, NULL, NULL, 0, 'RoleName', 'System', 'VOL.System', NULL, 8, NULL, NULL, 'Sys_Role', 'Sys_Role', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (3, '字典数据', '字典数据', NULL, NULL, '字典明细', 'Sys_DictionaryList', NULL, 0, 'DicName', 'System', 'VOL.System', NULL, 11, NULL, NULL, 'Sys_Dictionary', 'Sys_Dictionary', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (4, '字典明细', '字典明细', 'SysDbContext', NULL, NULL, NULL, NULL, 0, NULL, 'System', 'VOL.System', NULL, 11, NULL, NULL, 'Sys_DictionaryList', 'Sys_DictionaryList', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (5, '系统日志', '系统日志', 'SysDbContext', NULL, NULL, NULL, NULL, 0, NULL, 'System', 'VOL.System', NULL, 10, NULL, NULL, 'Sys_Log', 'Sys_Log', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (6, NULL, '用户管理', 'SysDbContext', NULL, NULL, NULL, NULL, 0, 'UserName', 'System', 'VOL.System', NULL, 8, NULL, NULL, 'Sys_User', 'Sys_User', 'HeadImageUrl', 1);
INSERT INTO `Sys_TableInfo` VALUES (8, '用户基础信息', '用户基础信息', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'System', 'VOL.System', 200, 0, NULL, NULL, '无', NULL, NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (10, '日志管理', '日志管理', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'System', 'VOL.System', NULL, 0, NULL, '170', '日志管理', NULL, NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (11, '配置管理', '配置管理', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'System', 'VOL.System', NULL, 0, NULL, '250', '配置管理', NULL, NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (12, 'ReportDBTest', 'ReportDBTest', 'ReportDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'DBTest', 'VOL.ReportTest', NULL, 0, NULL, 'CreateDate', 'ReportDBTest', 'ReportDBTest', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (14, 'ServiceDbTest', 'ServiceDbTest', 'ServiceDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'DbTest', 'VOL.ServiceTest', NULL, 0, NULL, 'CreateDate', 'ServiceDbTest', 'ServiceDbTest', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (15, '定时任务', '定时任务', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, '定时任务', 'VOL.System', NULL, 0, NULL, NULL, '定时任务', NULL, NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (16, '定时任务', '定时任务', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'Quartz', 'VOL.System', NULL, 15, NULL, 'CreateDate', 'Sys_QuartzOptions', 'Sys_QuartzOptions', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (17, '执行记录', '执行记录', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'Quartz', 'VOL.System', NULL, 15, NULL, 'CreateDate', 'Sys_QuartzLog', 'Sys_QuartzLog', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (18, '审批流程', '审批流程', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, '审批流程', 'VOL.System', NULL, 0, NULL, NULL, '审批流程', NULL, NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (19, '审批节点', '审批节点', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'flow', 'VOL.System', NULL, 18, NULL, 'CreateDate', 'Sys_WorkFlowTableStep', 'Sys_WorkFlowTableStep', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (20, '审批流程', '审批流程', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'flow', 'VOL.System', NULL, 18, NULL, 'CreateDate', 'Sys_WorkFlowTable', 'Sys_WorkFlowTable', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (21, '审批节点配置', '审批节点配置', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'flow', 'VOL.System', NULL, 18, NULL, 'CreateDate', 'Sys_WorkFlowStep', 'Sys_WorkFlowStep', NULL, NULL);
INSERT INTO `Sys_TableInfo` VALUES (22, '审批流程配置', '审批流程配置', 'SysDbContext', NULL, NULL, NULL, NULL, 1, NULL, 'flow', 'VOL.System', NULL, 18, NULL, 'CreateDate', 'Sys_WorkFlow', 'Sys_WorkFlow', NULL, NULL);

-- ----------------------------
-- Table structure for Sys_User
-- ----------------------------
DROP TABLE IF EXISTS `Sys_User`;
CREATE TABLE `Sys_User`  (
  `User_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Address` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `AppType` int(11) NULL DEFAULT NULL,
  `AuditDate` timestamp(6) NULL DEFAULT NULL,
  `AuditStatus` int(11) NULL DEFAULT NULL,
  `Auditor` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `CreateDate` timestamp(6) NULL DEFAULT NULL,
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `DeptName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Dept_Id` int(11) NULL DEFAULT NULL,
  `Email` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Enable` int(11) NULL DEFAULT NULL,
  `Gender` int(11) NULL DEFAULT NULL,
  `HeadImageUrl` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `IsRegregisterPhone` int(11) NULL DEFAULT NULL,
  `LastLoginDate` timestamp(6) NULL DEFAULT NULL,
  `LastModifyPwdDate` timestamp(6) NULL DEFAULT NULL,
  `Mobile` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Modifier` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `ModifyDate` timestamp(6) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  `OrderNo` int(11) NULL DEFAULT NULL,
  `Role_Id` int(11) NULL DEFAULT NULL,
  `RoleName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `PhoneNo` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Remark` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Tel` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UserName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UserPwd` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `UserTrueName` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `Token` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`User_Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3369 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Sys_User
-- ----------------------------
INSERT INTO `Sys_User` VALUES (1, '北京市西城区', 1, '2019-08-18 00:54:06.000000', 1, '超级管理员', '2012-06-10 11:10:03.000000', NULL, NULL, NULL, 0, '283591387@qq.com', 1, 1, 'Upload/Tables/Sys_User/202004271001535915/04.jpg', 0, '2017-08-28 09:58:55.000000', '2019-12-14 15:14:22.000000', NULL, '超级管理员', '2020-04-27 10:02:10.000000', 1, 0, 1, '超级管理员', '13888888888', '~还没想好...', NULL, 'admin', 'tyYANoQvblWIItqRe30Ivw==', '超级管理员', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxIiwiaWF0IjoiMTY3Njk5ODI3MCIsIm5iZiI6IjE2NzY5OTgyNzAiLCJleHAiOiIxNjc3MDA1NDcwIiwiaXNzIjoidm9sLmNvcmUub3duZXIiLCJhdWQiOiJ2b2wuY29yZSJ9.1X9OF4jxOndbkK8ClGL76bhZHEoxsoSUCRCz5A5pLQo');
INSERT INTO `Sys_User` VALUES (3362, '北京市还没注册', 1, '2019-08-18 00:54:06.000000', 1, '超级管理员', '2019-08-13 14:24:27.000000', NULL, NULL, NULL, NULL, NULL, 0, 0, 'Upload/Tables/Sys_User/201912141113355553/07.jpg', 1, NULL, '2019-09-22 23:03:33.000000', '01012345678', 'zs', '2020-05-01 16:37:19.000000', 3362, NULL, 2, '测试管理员', NULL, 'null', NULL, 'admin666', 'j79rYYvCz4vdhcboB1Ausg==', 'zs', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzMzYyIiwiaWF0IjoiMTY1MTYzMjYxOSIsIm5iZiI6IjE2NTE2MzI2MTkiLCJleHAiOiIxNjUxNjM5ODE5IiwiaXNzIjoidm9sLmNvcmUub3duZXIiLCJhdWQiOiJ2b2wuY29yZSJ9.poJl3WQCoC-7l5BjU0SOKlVLpmG0aBeZAbMd7hGMnQI');
INSERT INTO `Sys_User` VALUES (3368, NULL, NULL, NULL, NULL, NULL, '2020-04-26 17:20:41.000000', 1, '超级管理员', NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL, 'zs', '2020-05-01 16:34:25.000000', 3362, NULL, 2, '测试管理员', NULL, NULL, NULL, 'user2_1', 'FVUjlxZXEhBaFk1425xDSw==', '角色帐号2_1', NULL);

-- ----------------------------
-- Table structure for Sys_WorkFlow
-- ----------------------------
DROP TABLE IF EXISTS `Sys_WorkFlow`;
CREATE TABLE `Sys_WorkFlow`  (
  `WorkFlow_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `WorkName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程名称',
  `WorkTable` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表名',
  `WorkTableName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '功能菜单',
  `NodeConfig` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '节点信息',
  `LineConfig` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '连接配置',
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CreateDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Enable` tinyint(4) NULL DEFAULT NULL COMMENT '是否启用',
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`WorkFlow_Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_WorkFlow
-- ----------------------------
INSERT INTO `Sys_WorkFlow` VALUES ('c016fa5e-6f44-4d59-a929-7391e82caf18', '订单流程测试', 'SellOrder', '销售订单', '[{\"id\":\"1659276275052\",\"name\":\"流程-节点A\",\"type\":\"task\",\"left\":\"230px\",\"top\":\"15px\",\"ico\":\"el-icon-user-solid\",\"nodeType\":\"1\",\"userId\":1,\"roleId\":null},{\"id\":\"1659276282115\",\"name\":\"流程-节点B\",\"type\":\"task\",\"left\":\"228px\",\"top\":\"127px\",\"ico\":\"el-icon-goods\",\"nodeType\":\"1\",\"userId\":3362,\"roleId\":null},{\"id\":\"l0om4eidz\",\"name\":\"流程-节点C\",\"type\":\"timer\",\"left\":\"226px\",\"top\":\"243.25px\",\"ico\":\"el-icon-plus\",\"state\":\"success\",\"nodeType\":\"1\",\"userId\":3378,\"roleId\":null}]', '[{\"from\":\"1659276275052\",\"to\":\"1659276282115\"},{\"from\":\"1659276282115\",\"to\":\"l0om4eidz\"}]', '订单流程测试', '2022-08-15 05:00:03', 1, '超级管理员', 0, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlow` VALUES ('d3445da2-043f-4c8b-943a-0c8a8f92d4b5', '流程测试', 'App_Expert', '启用图片支持', '[{\"id\":\"1659276275052\",\"name\":\"流程C-节点A\",\"type\":\"task\",\"left\":\"46px\",\"top\":\"28px\",\"ico\":\"el-icon-user-solid\",\"nodeType\":\"1\",\"userId\":1,\"roleId\":null},{\"id\":\"1659276282115\",\"name\":\"流程C-节点B\",\"type\":\"task\",\"left\":\"61px\",\"top\":\"195px\",\"ico\":\"el-icon-goods\",\"nodeType\":\"1\",\"userId\":1,\"roleId\":null},{\"id\":\"txpo1vyv8u\",\"name\":\"添加节点\",\"type\":\"timer\",\"left\":\"266px\",\"top\":\"99px\",\"ico\":\"el-icon-plus\",\"state\":\"success\",\"stepValue\":null,\"nodeType\":1,\"userId\":3362,\"roleId\":null,\"deptId\":null},{\"id\":\"yshtxdrq9u\",\"name\":\"添加节点1\",\"type\":\"timer\",\"left\":\"498px\",\"top\":\"200px\",\"ico\":\"el-icon-plus\",\"state\":\"success\",\"stepValue\":null,\"nodeType\":1,\"userId\":3378,\"roleId\":null,\"deptId\":null},{\"id\":\"64q19orr1h\",\"name\":\"添加节点2\",\"type\":\"timer\",\"left\":\"515px\",\"top\":\"39px\",\"ico\":\"el-icon-plus\",\"state\":\"success\",\"stepValue\":null,\"nodeType\":1,\"userId\":1,\"roleId\":null,\"deptId\":null}]', '[{\"from\":\"1659276275052\",\"to\":\"1659276282115\"},{\"from\":\"1659276282115\",\"to\":\"txpo1vyv8u\"},{\"from\":\"txpo1vyv8u\",\"to\":\"yshtxdrq9u\"},{\"from\":\"yshtxdrq9u\",\"to\":\"64q19orr1h\"}]', '流程测试', '2022-08-15 05:02:05', 1, '超级管理员', 0, '超级管理员', '2022-08-17 00:30:38', 1);

-- ----------------------------
-- Table structure for Sys_WorkFlowStep
-- ----------------------------
DROP TABLE IF EXISTS `Sys_WorkFlowStep`;
CREATE TABLE `Sys_WorkFlowStep`  (
  `WorkStepFlow_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `WorkFlow_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程主表id',
  `StepId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程节点Id',
  `StepName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `StepType` int(11) NULL DEFAULT NULL COMMENT '节点类型(1=按用户审批,2=按角色审批,3=按部门审批)',
  `StepValue` int(11) NULL DEFAULT NULL COMMENT '审批用户id或角色id、部门id',
  `OrderId` int(11) NULL DEFAULT NULL COMMENT '审批顺序',
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CreateDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Enable` tinyint(4) NULL DEFAULT NULL,
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`WorkStepFlow_Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_WorkFlowStep
-- ----------------------------
INSERT INTO `Sys_WorkFlowStep` VALUES ('08da7e37-f54f-4d01-841c-fd9981caf52c', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276275052', '流程-节点A', 1, 1, 1, NULL, '2022-08-15 05:00:03', 1, '超级管理员', NULL, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowStep` VALUES ('08da7e37-f54f-4d43-8759-ea887bb9ad66', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276282115', '流程-节点B', 1, 3362, 2, NULL, '2022-08-15 05:00:03', 1, '超级管理员', NULL, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowStep` VALUES ('08da7e37-f54f-4d6a-8e9b-5a067e559d4c', 'c016fa5e-6f44-4d59-a929-7391e82caf18', 'l0om4eidz', '流程-节点C', 1, 3378, 3, NULL, '2022-08-15 05:00:03', 1, '超级管理员', NULL, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowStep` VALUES ('08da7e38-3e0b-48c7-8c31-702b73e9d97e', 'd3445da2-043f-4c8b-943a-0c8a8f92d4b5', '1659276275052', '流程C-节点A', 1, 1, 1, NULL, '2022-08-15 05:02:05', 1, '超级管理员', NULL, '超级管理员', '2022-08-17 00:30:38', 1);
INSERT INTO `Sys_WorkFlowStep` VALUES ('08da7e38-3e0b-48ef-8010-b941874857b5', 'd3445da2-043f-4c8b-943a-0c8a8f92d4b5', '1659276282115', '流程C-节点B', 1, 1, 2, NULL, '2022-08-15 05:02:05', 1, '超级管理员', NULL, '超级管理员', '2022-08-17 00:30:38', 1);
INSERT INTO `Sys_WorkFlowStep` VALUES ('257cb391-4b84-41eb-b8db-d38c6dde4e9c', 'd3445da2-043f-4c8b-943a-0c8a8f92d4b5', 'txpo1vyv8u', '添加节点', 1, 3362, 3, NULL, '2022-08-17 00:30:38', 1, '超级管理员', NULL, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowStep` VALUES ('a6b7408c-9dc4-422d-8d98-c9a5660f579a', 'd3445da2-043f-4c8b-943a-0c8a8f92d4b5', 'yshtxdrq9u', '添加节点1', 1, 3378, 4, NULL, '2022-08-17 00:30:38', 1, '超级管理员', NULL, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowStep` VALUES ('ee385e1c-d78a-4eb8-9539-661822dcbfb8', 'd3445da2-043f-4c8b-943a-0c8a8f92d4b5', '64q19orr1h', '添加节点2', 1, 1, 5, NULL, '2022-08-17 00:30:38', 1, '超级管理员', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for Sys_WorkFlowTable
-- ----------------------------
DROP TABLE IF EXISTS `Sys_WorkFlowTable`;
CREATE TABLE `Sys_WorkFlowTable`  (
  `WorkFlowTable_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `WorkFlow_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程id',
  `WorkName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `WorkTableKey` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表主键id',
  `WorkTable` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表名',
  `WorkTableName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务名称',
  `CurrentOrderId` int(11) NULL DEFAULT NULL COMMENT '当前审批节点',
  `AuditStatus` int(11) NULL DEFAULT NULL COMMENT '审批状态',
  `CreateDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `Enable` tinyint(4) NULL DEFAULT NULL,
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_WorkFlowTable
-- ----------------------------
INSERT INTO `Sys_WorkFlowTable` VALUES ('460b5903-4ff2-4f84-a88f-bbbf473942ec', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '订单流程测试', 'ec217c01-42a2-435f-bdb6-70613b947bf9', 'SellOrder', '销售订单', 2, 1, '2022-08-15 05:01:04', 1, '超级管理员', 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTable` VALUES ('a251f602-f8d1-4399-8429-48d3349ad210', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '订单流程测试', '978ad775-77c0-49ca-be31-ba36bb6f8af8', 'SellOrder', '销售订单', 2, 0, '2022-08-17 23:27:48', 1, '超级管理员', 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTable` VALUES ('b4d10013-8297-421f-aad4-3147fdac2450', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '订单流程测试', '47e41e06-cb4a-4763-9aeb-df66cd6615e1', 'SellOrder', '销售订单', 2, 0, '2022-08-16 02:03:17', 1, '超级管理员', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for Sys_WorkFlowTableStep
-- ----------------------------
DROP TABLE IF EXISTS `Sys_WorkFlowTableStep`;
CREATE TABLE `Sys_WorkFlowTableStep`  (
  `Sys_WorkFlowTableStep_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `WorkFlowTable_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主表id',
  `WorkFlow_Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程id',
  `StepId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节点id',
  `StepName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节名称',
  `StepType` int(11) NULL DEFAULT NULL COMMENT '审批类型',
  `StepValue` int(11) NULL DEFAULT NULL COMMENT '节点类型(1=按用户审批,2=按角色审批,3=按部门审批 )',
  `OrderId` int(11) NULL DEFAULT NULL COMMENT '审批顺序',
  `AuditId` int(11) NULL DEFAULT NULL COMMENT '审核人id',
  `Auditor` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核人',
  `AuditStatus` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  `AuditDate` datetime(0) NULL DEFAULT NULL COMMENT '审核时间',
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateDate` datetime(0) NULL DEFAULT NULL,
  `CreateID` int(11) NULL DEFAULT NULL,
  `Creator` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Enable` tinyint(4) NULL DEFAULT NULL,
  `Modifier` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ModifyDate` datetime(0) NULL DEFAULT NULL,
  `ModifyID` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of Sys_WorkFlowTableStep
-- ----------------------------
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('136b08e5-1fb5-4a07-8d7f-6f78d09c2685', '460b5903-4ff2-4f84-a88f-bbbf473942ec', 'c016fa5e-6f44-4d59-a929-7391e82caf18', 'l0om4eidz', '流程-节点C', 1, 3378, 3, 3378, NULL, NULL, NULL, NULL, '2022-08-15 05:01:04', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('1ac547df-27cf-4eb4-9ad6-37881ef671cc', 'a251f602-f8d1-4399-8429-48d3349ad210', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276282115', '流程-节点B', 1, 3362, 2, 3362, NULL, NULL, NULL, NULL, '2022-08-17 23:27:48', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('21c8252a-15a3-4435-a6ca-0dbee4474bba', 'b4d10013-8297-421f-aad4-3147fdac2450', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276275052', '流程-节点A', 1, 1, 1, 1, '超级管理员', 1, '2022-08-16 02:03:38', '审批通过测试', '2022-08-16 02:03:17', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('521f23e3-6d5d-4a9d-8e67-a289440f7ecd', 'a251f602-f8d1-4399-8429-48d3349ad210', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276275052', '流程-节点A', 1, 1, 1, 1, '超级管理员', 1, '2022-08-29 09:56:27', '555555555', '2022-08-17 23:27:48', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('81856e1e-9796-4ceb-8b8a-36b201604e25', '460b5903-4ff2-4f84-a88f-bbbf473942ec', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276275052', '流程-节点A', 1, 1, 1, 1, '超级管理员', 1, '2022-08-15 05:13:51', '1', '2022-08-15 05:01:04', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('83485f9f-119b-4344-a26a-22a1f4a3760e', 'b4d10013-8297-421f-aad4-3147fdac2450', 'c016fa5e-6f44-4d59-a929-7391e82caf18', 'l0om4eidz', '流程-节点C', 1, 3378, 3, 3378, NULL, NULL, NULL, NULL, '2022-08-16 02:03:17', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('d9a0f59c-7b3e-4099-84a5-2b3c74414b46', 'a251f602-f8d1-4399-8429-48d3349ad210', 'c016fa5e-6f44-4d59-a929-7391e82caf18', 'l0om4eidz', '流程-节点C', 1, 3378, 3, 3378, NULL, NULL, NULL, NULL, '2022-08-17 23:27:48', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('e623c47c-e0f5-4052-a2be-baf857784499', '460b5903-4ff2-4f84-a88f-bbbf473942ec', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276282115', '流程-节点B', 1, 3362, 2, 3362, NULL, NULL, NULL, NULL, '2022-08-15 05:01:04', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `Sys_WorkFlowTableStep` VALUES ('f92d5a23-f572-446e-a3e4-e6063ee6dcdf', 'b4d10013-8297-421f-aad4-3147fdac2450', 'c016fa5e-6f44-4d59-a929-7391e82caf18', '1659276282115', '流程-节点B', 1, 3362, 2, 3362, NULL, NULL, NULL, NULL, '2022-08-16 02:03:17', NULL, NULL, 1, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
